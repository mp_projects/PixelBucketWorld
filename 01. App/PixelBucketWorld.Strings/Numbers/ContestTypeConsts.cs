﻿namespace PixelBucketWorld.Consts.Numbers
{
    public class ContestTypeConsts
    {
        public const int OPEN = 1;
        public const int INVITATIONAL = 2;
    }
}
