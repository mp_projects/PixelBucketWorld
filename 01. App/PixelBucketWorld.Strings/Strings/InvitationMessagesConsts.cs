﻿namespace PixelBucketWorld.Consts.Strings
{
    public static class InvitationMessagesConsts
    {
        public static string TITLE_PARTICIPANT = "Contest participation invitation!";

        /// <summary>
        /// The content participant
        /// </summary>
        /// <param name="0">username</param>
        /// <param name="1">contest title</param>
        public static string CONTENT_PARTICIPANT = "Congratulations {0}! You have been invited to participate in the new contest {1} in the wild";

        public static string TITLE_JUROR = "Contest juror invitation!";

        /// <summary>
        /// The content juror
        /// </summary>
        /// <param name="0">username</param>
        /// <param name="1">contest title</param>
        public static string CONTENT_JUROR = "Congratulations {0}! You have been invited to be a juror for the new contest {1}";


    }
}
