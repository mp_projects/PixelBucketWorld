﻿using System;

namespace PixelBucketWorld.Consts.Strings
{
    public static class LogLevelConsts
    {
        public static string INFORMATION = "Information";
        public static string ERROR = "Error";
        public static string WARNING = "Warning";
    }
}
