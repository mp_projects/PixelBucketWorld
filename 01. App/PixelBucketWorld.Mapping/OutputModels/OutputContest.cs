﻿using Foolproof;
using PixeBucketWorld.Data.ValidationAttributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.Json.Serialization;

namespace PixelBucketWorld.Map.OutputModels
{
    public class OutputContest
    {
        [JsonIgnore]
        public int Id { get; set; }
        public string Title { get; set; }

        public int CategoryId { get; set; }

        public int TypeId { get; set; }

        public DateTime PhaseOneStartingDate { get; set; }

        public DateTime PhaseOneFinishingDate { get; set; }

        public DateTime PhaseTwoStartingDate { get; set; }

        public DateTime PhaseTwoFinishingDate { get; set; }
    }
}
