﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PixeBucketWorld.Data;
using PixeBucketWorld.Data.Models;
using PixelBucketWorld.Consts.Numbers;
using PixelBucketWorld.Services;
using PixelBucketWorld.Services.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PixelBucketWorld.Tests.ContestServiceTests
{
    [TestClass]
    public class SwitchPhases_Should
    {
        [TestMethod]
        [DataRow(-1, PhaseConsts.PRESTART, PhaseConsts.PRESTART)]
        [DataRow(1, PhaseConsts.PRESTART, PhaseConsts.PHASE_ONE)]
        [DataRow(1, PhaseConsts.PHASE_ONE, PhaseConsts.PHASE_ONE)]
        [DataRow(2.5, PhaseConsts.PHASE_ONE, PhaseConsts.TRANSITION)]
        [DataRow(2.5, PhaseConsts.TRANSITION, PhaseConsts.TRANSITION)]
        [DataRow(3.5, PhaseConsts.TRANSITION, PhaseConsts.PHASE_TWO)]
        [DataRow(3.5, PhaseConsts.PHASE_TWO, PhaseConsts.PHASE_TWO)]
        [DataRow(4.5, PhaseConsts.PHASE_TWO, PhaseConsts.FINISHED)]
        [DataRow(4.5, PhaseConsts.FINISHED, PhaseConsts.FINISHED)]
        public async Task SwitchPhases_Should_SwitchCorrectlyOneContest(double addDays, int currentPhase, int expectedPhase)
        {
            // Arrange

            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new PBWDbContext(options))
            {
                context.Database.EnsureDeleted();

                var contest = new Contest()
                {
                    Id = 1, Name = "1",
                    PhaseOneStartingDate = DateTime.Now.AddDays(0),
                    PhaseOneFinishingDate = DateTime.Now.AddDays(2),
                    PhaseTwoStartingDate = DateTime.Now.AddDays(3),
                    PhaseTwoFinishingDate = DateTime.Now.AddDays(4),
                    PhaseId = currentPhase
                };

                context.Contests.Add(contest);
                
                context.SaveChanges();

                var unitOfWork = new UnitOfWork(context);

                // Act
                await unitOfWork.ContestService.SwitchPhasesAsync(DateTime.Now.AddDays(addDays));

                // Assert
                Assert.AreEqual(expectedPhase, contest.PhaseId);

            }
        }

        [TestMethod]
        [DataRow(-1, PhaseConsts.PRESTART, 0)]
        [DataRow(1, PhaseConsts.PRESTART, 1)]
        [DataRow(1, PhaseConsts.PHASE_ONE, 0)]
        [DataRow(2.5, PhaseConsts.PHASE_ONE, 1)]
        [DataRow(2.5, PhaseConsts.TRANSITION, 0)]
        [DataRow(3.5, PhaseConsts.TRANSITION, 1)]
        [DataRow(3.5, PhaseConsts.PHASE_TWO, 0)]
        [DataRow(4.5, PhaseConsts.PHASE_TWO, 1)]
        [DataRow(4.5, PhaseConsts.FINISHED, 0)]
        public async Task SwitchPhases_Should_ReturnOnlySwitchedContests(double addDays, int currentPhase, int expectedCount)
        {
            // Arrange

            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new PBWDbContext(options))
            {
                context.Database.EnsureDeleted();

                var contest = new Contest()
                {
                    Id = 1,
                    Name = "1",
                    PhaseOneStartingDate = DateTime.Now.AddDays(0),
                    PhaseOneFinishingDate = DateTime.Now.AddDays(2),
                    PhaseTwoStartingDate = DateTime.Now.AddDays(3),
                    PhaseTwoFinishingDate = DateTime.Now.AddDays(4),
                    PhaseId = currentPhase
                };

                context.Contests.Add(contest);

                context.SaveChanges();

                var unitOfWork = new UnitOfWork(context);

                // Act
                var switchedContests = await unitOfWork.ContestService.SwitchPhasesAsync(DateTime.Now.AddDays(addDays));

                // Assert
                Assert.AreEqual(expectedCount, switchedContests.Count);

            }
        }



    }
}
