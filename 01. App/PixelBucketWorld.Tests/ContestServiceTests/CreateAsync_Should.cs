﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PixeBucketWorld.Data;
using PixeBucketWorld.Data.Models;
using PixelBucketWorld.Services;
using System;
using System.Data;
using System.Threading.Tasks;

namespace PixelBucketWorld.Tests.ContestServiceTests
{
    [TestClass]
    public class CreateAsync_Should
    {
        [DataTestMethod]
        public async Task CreateAsync_Should_Throw_IfDuplicateName()
        {
            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new PBWDbContext(options))
            {
                // Seed and save the database
                Util.SeedDatabase(context);

                var unitOfWork = new UnitOfWork(context);

                var newContest = new Contest()
                {
                    Id = 999,
                    CategoryId = 3,
                    Name = "Small Boats",
                    PhaseId = 1,
                    TypeId = 1,
                    PhaseOneStartingDate = DateTime.Now.AddDays(-10),
                    PhaseOneFinishingDate = DateTime.Now.AddDays(-9),
                    PhaseTwoStartingDate = DateTime.Now.AddDays(-8),
                    PhaseTwoFinishingDate = DateTime.Now.AddDays(-7)
                };

                // Act & Assert
                await Assert.ThrowsExceptionAsync<DuplicateNameException>(
                    () => unitOfWork.ContestService.CreateAsync(newContest));
            }

        }

        [DataTestMethod]
        public async Task CreateAsync_Should_Throw_InvalidCategoryId()
        {
            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new PBWDbContext(options))
            {
                // Seed and save the database
                Util.SeedDatabase(context);

                var unitOfWork = new UnitOfWork(context);

                var newContest = new Contest()
                {
                    Id = 999,
                    CategoryId = 999,
                    Name = "Contest 999",
                    PhaseId = 1,
                    TypeId = 1,
                    PhaseOneStartingDate = DateTime.Now.AddDays(-10),
                    PhaseOneFinishingDate = DateTime.Now.AddDays(-9),
                    PhaseTwoStartingDate = DateTime.Now.AddDays(-8),
                    PhaseTwoFinishingDate = DateTime.Now.AddDays(-7)
                };

                // Act & Assert
                await Assert.ThrowsExceptionAsync<ArgumentOutOfRangeException>(
                    () => unitOfWork.ContestService.CreateAsync(newContest));
            }

        }

        [DataTestMethod]
        public async Task CreateAsync_Should_Throw_InvalidTypeId()
        {
            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new PBWDbContext(options))
            {
                // Seed and save the database
                Util.SeedDatabase(context);

                var unitOfWork = new UnitOfWork(context);

                var newContest = new Contest()
                {
                    Id = 999,
                    CategoryId = 999,
                    Name = "Contest 999",
                    PhaseId = 1,
                    TypeId = 3,
                    PhaseOneStartingDate = DateTime.Now.AddDays(-10),
                    PhaseOneFinishingDate = DateTime.Now.AddDays(-9),
                    PhaseTwoStartingDate = DateTime.Now.AddDays(-8),
                    PhaseTwoFinishingDate = DateTime.Now.AddDays(-7)
                };

                // Act & Assert
                await Assert.ThrowsExceptionAsync<ArgumentOutOfRangeException>(
                    () => unitOfWork.ContestService.CreateAsync(newContest));
            }

        }

        [DataTestMethod]
        public async Task CreateAsync_Should_Throw_InvalidPhaseId()
        {
            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new PBWDbContext(options))
            {
                // Seed and save the database
                Util.SeedDatabase(context);

                var unitOfWork = new UnitOfWork(context);

                var newContest = new Contest()
                {
                    Id = 999,
                    CategoryId = 999,
                    Name = "Contest 999",
                    PhaseId = 5,
                    TypeId = 3,
                    PhaseOneStartingDate = DateTime.Now.AddDays(-10),
                    PhaseOneFinishingDate = DateTime.Now.AddDays(-9),
                    PhaseTwoStartingDate = DateTime.Now.AddDays(-8),
                    PhaseTwoFinishingDate = DateTime.Now.AddDays(-7)
                };

                // Act & Assert
                await Assert.ThrowsExceptionAsync<ArgumentOutOfRangeException>(
                    () => unitOfWork.ContestService.CreateAsync(newContest));
            }

        }

        [DataTestMethod]
        public async Task CreateAsync_Should_AddToContext_IfValidModel()
        {
            // Arrange
            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new PBWDbContext(options))
            {
                // Seed and save the database
                Util.SeedDatabase(context);

                var unitOfWork = new UnitOfWork(context);

                var newContest = new Contest()
                {
                    //Id = 999,
                    CategoryId = 1,
                    Name = "Contest 999",
                    PhaseId = 1,
                    TypeId = 1,
                    PhaseOneStartingDate = DateTime.Now.AddDays(1),
                    PhaseOneFinishingDate = DateTime.Now.AddDays(2),
                    PhaseTwoStartingDate = DateTime.Now.AddDays(3),
                    PhaseTwoFinishingDate = DateTime.Now.AddDays(4)
                };

                // Act
                await unitOfWork.ContestService.CreateAsync(newContest);

                var state = context.Entry(newContest).State;

                // Assert
                Assert.IsTrue(state == Microsoft.EntityFrameworkCore.EntityState.Added);
            }

        }

        [DataTestMethod]
        public async Task CreateAsync_Should_ReturnTheCreatedObject_IfValidModel()
        {
            // Arrange
            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new PBWDbContext(options))
            {
                // Seed and save the database
                Util.SeedDatabase(context);

                var unitOfWork = new UnitOfWork(context);

                var newContest = new Contest()
                {
                    CategoryId = 1,
                    Name = "Contest 999",
                    PhaseId = 1,
                    TypeId = 1,
                    PhaseOneStartingDate = DateTime.Now.AddDays(1),
                    PhaseOneFinishingDate = DateTime.Now.AddDays(2),
                    PhaseTwoStartingDate = DateTime.Now.AddDays(3),
                    PhaseTwoFinishingDate = DateTime.Now.AddDays(4)
                };

                // Act
                Contest returnedContest = await unitOfWork.ContestService.CreateAsync(newContest);

                // Assert
                Assert.AreEqual(newContest.CategoryId, returnedContest.CategoryId);
                Assert.AreEqual(newContest.Name, returnedContest.Name);
                Assert.AreEqual(newContest.PhaseId, returnedContest.PhaseId);
                Assert.AreEqual(newContest.TypeId, returnedContest.TypeId);
                Assert.AreEqual(newContest.PhaseOneStartingDate, returnedContest.PhaseOneStartingDate);
                Assert.AreEqual(newContest.PhaseOneFinishingDate, returnedContest.PhaseOneFinishingDate);
                Assert.AreEqual(newContest.PhaseTwoStartingDate, returnedContest.PhaseTwoStartingDate);
                Assert.AreEqual(newContest.PhaseTwoFinishingDate, returnedContest.PhaseTwoFinishingDate);
            }

        }

    }
}
