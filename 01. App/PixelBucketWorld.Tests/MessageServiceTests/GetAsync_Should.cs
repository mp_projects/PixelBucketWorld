﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PixeBucketWorld.Data;
using PixelBucketWorld.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PixelBucketWorld.Tests.MessageServiceTests
{
    [TestClass]
    public class GetAsync_Should
    {
        [TestMethod]
        public async Task GetAsync_ShouldReturnAllMessages_IfDefaultParametersAreUsed()
        {
            var options = Util.GetDbContextOptions(StringConsts.TEST_DB);

            using (var context = new PBWDbContext(options))
            {
                Util.SeedDatabase(context);

                var unitOfWork = new UnitOfWork(context);

                var messages = await unitOfWork.MessageService.GetAsync();

                Assert.AreEqual(Util.SeedMessages().Count(), messages.Count());
            }
        }

        [TestMethod]
        public async Task GetAsync_ShouldReturnAllUserMessages_IfUserIdIsPassed()
        {
            var options = Util.GetDbContextOptions(StringConsts.TEST_DB);

            using (var context = new PBWDbContext(options))
            {
                Util.SeedDatabase(context);

                var unitOfWork = new UnitOfWork(context);

                var messages = await unitOfWork.MessageService.GetAsync(3);

                Assert.IsTrue(messages.All(m => m.ReceiverId == 3));
            }
        }

        [TestMethod]
        public async Task GetAsync_ShouldReturnAllMessagesForConcreteContest_IfContestIdIsPassed()
        {
            var options = Util.GetDbContextOptions(StringConsts.TEST_DB);

            using (var context = new PBWDbContext(options))
            {
                Util.SeedDatabase(context);

                var unitOfWork = new UnitOfWork(context);

                var messages = await unitOfWork.MessageService.GetAsync(null, 1);

                Assert.IsTrue(messages.All(m => m.ContestId == 1));
            }
        }

        [TestMethod]
        public async Task GetAsync_ShouldReturnAllUserMessagesForConcreteContest_IfUserIdAndContestIdArePassed()
        {
            var options = Util.GetDbContextOptions(StringConsts.TEST_DB);

            using (var context = new PBWDbContext(options))
            {
                Util.SeedDatabase(context);

                var unitOfWork = new UnitOfWork(context);

                var messages = await unitOfWork.MessageService.GetAsync(3, 1);

                Assert.IsTrue(messages.All(m => m.ReceiverId == 3 && m.ContestId == 1));
            }
        }

        [TestMethod]
        public async Task GetAsync_ShouldReturnEmptyCollection_IfInvalidUserIdIsPassed()
        {
            var options = Util.GetDbContextOptions(StringConsts.TEST_DB);

            using (var context = new PBWDbContext(options))
            {
                Util.SeedDatabase(context);

                var unitOfWork = new UnitOfWork(context);

                var messages = await unitOfWork.MessageService.GetAsync(1000);

                Assert.IsFalse(messages.Any());
            }
        }
    }
}
