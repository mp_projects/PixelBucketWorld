﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PixeBucketWorld.Data;
using PixelBucketWorld.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PixelBucketWorld.Tests.MessageServiceTests
{
    [TestClass]
    public class SendParticipantsInvitationalMessageAsync_Should
    {
        [TestMethod]
        public async Task SendParticipantsInvitationalMessageAsync_ShouldThrowInvalidOperationException_IfContestPhaseIsNotPrestart()
        {
            var options = Util.GetDbContextOptions(StringConsts.TEST_DB);

            using (var context = new PBWDbContext(options))
            {
                //Arrange
                Util.SeedDatabase(context);

                var unitOfWork = new UnitOfWork(context);

                var userIds = new List<int> { 2, 3 };

                var senderId = 1;

                var contestId = 2;

                var messagesCount = context.InvitationalMessages.Count();

                await Assert.ThrowsExceptionAsync<InvalidOperationException>(async () => await unitOfWork.MessageService.SendParticipantsInvitationalMessageAsync(userIds, contestId, senderId));

            }
        }

        [TestMethod]
        public async Task SendParticipantsInvitationalMessageAsync_ShouldThrowArgumentNullException_IfUserIdsAreNull()
        {
            var options = Util.GetDbContextOptions(StringConsts.TEST_DB);

            using (var context = new PBWDbContext(options))
            {
                //Arrange
                Util.SeedDatabase(context);

                var unitOfWork = new UnitOfWork(context);

                List<int> userIds = null;

                var senderId = 1;

                var contestId = 2;

                var messagesCount = context.InvitationalMessages.Count();

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(async () => await unitOfWork.MessageService.SendParticipantsInvitationalMessageAsync(userIds, contestId, senderId));

            }
        }

        [TestMethod]
        public async Task SendParticipantsInvitationalMessageAsync_ShouldThrowArgumentNullException_IfInvalidUserIdIsPassed()
        {
            var options = Util.GetDbContextOptions(StringConsts.TEST_DB);

            using (var context = new PBWDbContext(options))
            {
                //Arrange
                Util.SeedDatabase(context);

                var unitOfWork = new UnitOfWork(context);

                List<int> userIds = new List<int> { 5000 };

                var senderId = 1;

                var contestId = 2;

                var messagesCount = context.InvitationalMessages.Count();

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(async () => await unitOfWork.MessageService.SendParticipantsInvitationalMessageAsync(userIds, contestId, senderId));
            }
        }

        [TestMethod]
        public async Task SendParticipantsInvitationalMessageAsync_ShouldThrowInvalidOperationException_IfOrganiserIdIsPassedInUserIds()
        {
            var options = Util.GetDbContextOptions(StringConsts.TEST_DB);

            using (var context = new PBWDbContext(options))
            {
                //Arrange
                Util.SeedDatabase(context);

                var unitOfWork = new UnitOfWork(context);

                List<int> userIds = new List<int> { 1 };

                var senderId = 1;

                var contestId = 2;

                var messagesCount = context.InvitationalMessages.Count();

                await Assert.ThrowsExceptionAsync<InvalidOperationException>(async () => await unitOfWork.MessageService.SendParticipantsInvitationalMessageAsync(userIds, contestId, senderId));
            }
        }

        [TestMethod]
        public async Task SendParticipantsInvitationalMessageAsync_ShouldThrowArgumentNullException_IfInvalidContestIdIsPassed()
        {
            var options = Util.GetDbContextOptions(StringConsts.TEST_DB);

            using (var context = new PBWDbContext(options))
            {
                //Arrange
                Util.SeedDatabase(context);

                var unitOfWork = new UnitOfWork(context);

                List<int> userIds = new List<int> { 2 };

                var senderId = 1;

                var contestId = 1000;

                var messagesCount = context.InvitationalMessages.Count();

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(async () => await unitOfWork.MessageService.SendParticipantsInvitationalMessageAsync(userIds, contestId, senderId));
            }
        }

        [TestMethod]
        public async Task SendParticipantsInvitationalMessageAsync_ShouldThrowInvalidOperationException_IfContestTypeIsNotInvitational()
        {
            var options = Util.GetDbContextOptions(StringConsts.TEST_DB);

            using (var context = new PBWDbContext(options))
            {
                //Arrange
                Util.SeedDatabase(context);

                var unitOfWork = new UnitOfWork(context);

                List<int> userIds = new List<int> { 2 };

                var senderId = 1;

                var contestId = 101;

                var messagesCount = context.InvitationalMessages.Count();

                await Assert.ThrowsExceptionAsync<InvalidOperationException>(async () => await unitOfWork.MessageService.SendParticipantsInvitationalMessageAsync(userIds, contestId, senderId));
            }
        }


        [TestMethod]
        public async Task SendParticipantsInvitationalMessageAsync_ShouldSendInvitationMessageToCorrectUser_IfValidParametersArePassed()
        {
            var options = Util.GetDbContextOptions(StringConsts.TEST_DB);

            using (var context = new PBWDbContext(options))
            {
                //Arrange
                Util.SeedDatabase(context);

                var unitOfWork = new UnitOfWork(context);

                List<int> userIds = new List<int> { 2 };

                var senderId = 1;

                var contestId = 4;

                var messagesCount = context.InvitationalMessages.Count();

                await unitOfWork.MessageService.SendParticipantsInvitationalMessageAsync(userIds, contestId, senderId);
                await unitOfWork.SaveAsync();

                Assert.AreEqual(2, context.InvitationalMessages.Last().ReceiverId);
            }
        }
    }
}
