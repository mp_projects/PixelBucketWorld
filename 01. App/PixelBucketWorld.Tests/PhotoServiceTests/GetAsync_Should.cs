﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PixeBucketWorld.Data;
using PixelBucketWorld.Services;
using System.Linq;
using System.Threading.Tasks;

namespace PixelBucketWorld.Tests.PhotoServiceTests
{
    [TestClass]
    public class GetAsync_Should
    {
        [TestMethod]
        [DataRow(null, 18)]
        [DataRow(1, 2)]
        [DataRow(3, 4)]
        public async Task GetAsync_Should_Return_Correctly(int? contestId, int expectedCount)
        {
            // Arrange
            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new PBWDbContext(options))
            {
                // Seed and save the database
                Util.SeedDatabase(context);

                var unitOfWork = new UnitOfWork(context);

                // Act
                var photos = await unitOfWork.PhotoService.GetAsync(contestId);

                // Assert
                Assert.AreEqual(expectedCount, photos.Count());
            }
        }

    }
}
