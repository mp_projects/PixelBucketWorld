﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PixeBucketWorld.Data;
using PixeBucketWorld.Data.Models;
using PixeBucketWorld.Data.Models.Logging.Enums;
using PixelBucketWorld.Services;
using PixelBucketWorld.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PixelBucketWorld.Tests.LoggingServiceTests
{
    [TestClass]
    public class LogInformationAsync_Should
    {
        [TestMethod]
        public async Task LogInformationAsync_ShouldAddLogEntryToLogs()
        {
            var options = Util.GetDbContextOptions(StringConsts.TEST_DB);

            using (var context = new PBWDbContext(options))
            {
                //Arrange
                Util.SeedDatabase(context);

                var loggingService = new LoggingService(context);
                var user = new User();

                //Act
                var currLogsCount = context.Logs.Count();
                await loggingService.LogInformationAsync(InformationLogEvent.INSERT_ITEM, null, user);

                //Assert
                Assert.AreEqual(currLogsCount + 1, context.Logs.Local.Count());
            }
        }

        [TestMethod]
        public async Task LogInformationAsync_ShouldThrowArgumentNullException_IfNullModelIsPassed()
        {
            var options = Util.GetDbContextOptions(StringConsts.TEST_DB);

            using (var context = new PBWDbContext(options))
            {
                //Arrange
                Util.SeedDatabase(context);

                var loggingService = new LoggingService(context);
                User user = null;

                //Act && Assert
                await Assert.ThrowsExceptionAsync<ArgumentNullException>
                     (async () => await loggingService.LogInformationAsync(InformationLogEvent.INSERT_ITEM, null, user));
            }
        }
    }
}
