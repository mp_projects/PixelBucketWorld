﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PixeBucketWorld.Data;
using PixelBucketWorld.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PixelBucketWorld.Tests.UserServiceTests
{
    [TestClass]
    public class GetPointsTillNextRank_Should
    {
        [TestMethod]
        public async Task GetPointsTillNextRank_ShouldReturnNull_OrganiserIdIsPassed()
        {
           var options = Util.GetDbContextOptions(StringConsts.TEST_DB);

            using (var context = new PBWDbContext(options))
            {
                Util.SeedDatabase(context);

                var unitOfWork = new UnitOfWork(context);

                //Act
                //organiser id
                var rankingPoints = await unitOfWork.UserService.GetPointsTillNextRankAsync(1);

                //Assert
                Assert.IsNull(rankingPoints);
            }
        }

        [TestMethod]
        public async Task GetPointsTillNextRank_ShouldReturnCorrectPoints_WhenParticipantIdIsPassed()
        {
            var options = Util.GetDbContextOptions(StringConsts.TEST_DB);

            using (var context = new PBWDbContext(options))
            {
                Util.SeedDatabase(context);

                var unitOfWork = new UnitOfWork(context);

                //Act
                var rankingPoints = await unitOfWork.UserService.GetPointsTillNextRankAsync(2);

                //Assert
                Assert.AreEqual(51, rankingPoints);
            }
        }
    }
}
