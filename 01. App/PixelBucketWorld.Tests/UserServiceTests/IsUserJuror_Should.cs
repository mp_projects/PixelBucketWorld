﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PixeBucketWorld.Data;
using PixelBucketWorld.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PixelBucketWorld.Tests.UserServiceTests
{
    [TestClass]
    public class IsUserJuror_Should
    {
        [TestMethod]
        public async Task IsUserJurorAsync_ShouldReturnTrue_IfUserIdIsEligibleJurorId()
        {
            var options = Util.GetDbContextOptions(StringConsts.TEST_DB);

            using (var context = new PBWDbContext(options))
            {
                //Arrange
                Util.SeedDatabase(context);

                var unitOfWork = new UnitOfWork(context);

                //Act
                var isJuror = await unitOfWork.UserService.IsUserJurorAsync(5);

                //Assert
                Assert.IsTrue(isJuror);
            }
        }

        [TestMethod]
        public async Task IsUserJurorAsync_ShouldReturnFalse_IfUserIdIsNotEligibleJurorId()
        {
            var options = Util.GetDbContextOptions(StringConsts.TEST_DB);

            using (var context = new PBWDbContext(options))
            {
                //Arrange
                Util.SeedDatabase(context);

                var unitOfWork = new UnitOfWork(context);

                //Act
                var isJuror = await unitOfWork.UserService.IsUserJurorAsync(6);

                //Assert
                Assert.IsFalse(isJuror);
            }
        }

        [TestMethod]
        public async Task IsUserJurorAsync_ShouldThrowArgumentNullException_IfInvalidUserIdIsPassed()
        {
            var options = Util.GetDbContextOptions(StringConsts.TEST_DB);

            using (var context = new PBWDbContext(options))
            {
                //Arrange
                Util.SeedDatabase(context);

                var unitOfWork = new UnitOfWork(context);

                //Act && Assert
                await Assert.ThrowsExceptionAsync<ArgumentNullException>(async () => await unitOfWork.UserService.IsUserJurorAsync(7));
            }
        }
    }
}
