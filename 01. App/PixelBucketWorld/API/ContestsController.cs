﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PixeBucketWorld.Data.Models;
using PixeBucketWorld.Data.Models.Logging.Enums;
using PixelBucketWorld.API.Helpers;
using PixelBucketWorld.Consts.Strings;
using PixelBucketWorld.Helpers;
using PixelBucketWorld.Helpers.Mappers.OutputModels;
using PixelBucketWorld.Helpers.Paging;
using PixelBucketWorld.Map;
using PixelBucketWorld.Map.InputModels.InputContestModels;
using PixelBucketWorld.Map.OutputModels;
using PixelBucketWorld.Services;
using PixelBucketWorld.Services.Contracts;
using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace PixelBucketWorld.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContestsController : ControllerBase
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly ILoggingService loggingService;

        public ContestsController(IUnitOfWork unitOfWork, ILoggingService loggingService)
        {
            this.unitOfWork = unitOfWork;
            this.loggingService = loggingService;
        }

        /// <summary>
        /// Organizers can create a new contest.
        /// Token authentication.
        /// </summary>
        /// <remarks>
        /// Sample request: POST api/contests
        /// </remarks>
        /// <param name="inputContest"></param>
        /// <response code="201">Returns the created contest and its url.</response>
        /// <response code="400">
        /// If a contest with the same name exists.
        /// If unexisting category.
        /// If unexisting context type.
        /// If pahse is not prestart.
        /// </response>
        /// <response code="401">If ivalid token.</response>
        [Authorize(UserRoleConsts.ORGANISER)]
        [HttpPost("")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> Create([FromForm] InputContestModel inputContest)
        {
            try
            {
                var file = inputContest.File;

                string dbUrlPath;
                if (file == null)
                {
                    dbUrlPath = $"/images/ContestPhotos/PixelBucketWorld.jpg";
                }
                else
                {
                    var folderName = Path.Combine("wwwroot", "images", "ContestPhotos");
                    var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
                    var fileName =
                        Guid.NewGuid().ToString() + "_" + ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');

                    var filePath = Path.Combine(pathToSave, fileName);
                    dbUrlPath = $"/images/ContestPhotos/{fileName}";

                    await Uploader.Upload(file, filePath);
                }

                Contest contest = InputMapper.Map(inputContest);
                contest.PhotoUrl = dbUrlPath;

                await this.unitOfWork.ContestService.CreateAsync(contest);
                await this.unitOfWork.SaveAsync();

                await this.loggingService.LogInformationAsync(InformationLogEvent.INSERT_ITEM, null, contest);
                await this.unitOfWork.SaveAsync();

                var route = Request.Path.Value;

                string url = UrlHelper.GetUrl(Request, contest.Id);

                return Created(url, new OutputContestPartial(contest));

            }
            catch (DuplicateNameException e)
            {
                return BadRequest(e.Message);
            }
            catch (ArgumentOutOfRangeException e)
            {
                return BadRequest(e.Message);
            }
            catch (InvalidOperationException e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Organizers can see information about a particuliar contest.
        /// Token authentication.
        /// </summary>
        /// <param name="id"></param>
        /// <response code="200">Returns the contest.</response>
        /// <response code="401">If invalid token.</response>
        /// <response code="404">If the conest is not found.</response>
        [Authorize(UserRoleConsts.ORGANISER)]
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetById(int id)
        {
            try
            {
                var contest = await this.unitOfWork.ContestService.GetByIdAsync(id);

                return Ok(new OutputContestFull(contest));
            }
            catch(ArgumentNullException)
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Oragnizers can get contests filtered by phase and/or type.
        /// Token authentication.
        /// </summary>
        /// <remarks>
        /// Possible values:
        /// 
        /// Phase Id: 1 - prestart; 2 - phase I; 3 - trasition; 4 - phase II; 5 - finished.
        /// 
        /// Type Id: 1 - open; 2 - invitational.
        /// </remarks>
        /// <param name="phaseId"></param>
        /// <param name="typeId"></param>
        /// <param name="pagingParameters"></param>
        /// <response code="200">Returns a collection of filtered contest.</response>
        /// <response code="401">If invalid token.</response>
        [Authorize(UserRoleConsts.ORGANISER)]
        [HttpGet("")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetByPhaseAndType([FromQuery] int? phaseId, int? typeId, [FromQuery] PagingParameters pagingParameters)
        {
            var contests = (await this.unitOfWork.ContestService.GetContestsByPhaseAndTypeAsync(phaseId: phaseId, typeId: typeId))
                .Select(c => new OutputContestPartial(c));

            var pagedContests = PagedList<OutputContestPartial>.ToPagedList
                (
                contests,
                pagingParameters.Pagenumber,
                pagingParameters.PageSize,
                Request
                );

            WriteHeaders.PagedResposeParameters(Response, pagedContests);

            return Ok(pagedContests);

        }

        /// <summary>
        /// Participants (and organizers) see all open contests.
        /// Token authentication.
        /// </summary>
        /// <param name="pagingParameters"></param>
        /// <response code="200">Returns a collection of all open contests.</response>
        /// <response code="401">If invalid token.</response>
        [Authenticate]
        [HttpGet("open")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetOpenContests([FromQuery] PagingParameters pagingParameters)
        {
            var contests = (await this.unitOfWork.ContestService.GetContestsByPhaseAndTypeAsync(typeId: 1))
                .Select(c => new OutputContestPartial(c));

            var pagedContests = PagedList<OutputContestPartial>.ToPagedList
                (
                contests,
                pagingParameters.Pagenumber,
                pagingParameters.PageSize,
                Request
                );

            WriteHeaders.PagedResposeParameters(Response, pagedContests);

            return Ok(pagedContests);
        }

        //[Authenticate]
        //[HttpGet("")]
        //public async Task<IActionResult> GetAll([FromQuery] PagingParameters pagingParameters)
        //{
        //    var contests = (await this.unitOfWork.ContestService.GetAllAsync()).Select(c => c.Name);

        //    var pagedContests = PagedList<string>.ToPagedList
        //        (
        //        contests,
        //        pagingParameters.Pagenumber,
        //        pagingParameters.PageSize,
        //        Request
        //        );

        //    WriteHeaders.PagedResposeParameters(Response, pagedContests);

        //    return Ok(pagedContests);
        //}

    }
}
