﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PixeBucketWorld.Data.Models;
using PixeBucketWorld.Data.Models.Logging.Enums;
using PixelBucketWorld.API.Helpers;
using PixelBucketWorld.Consts.Strings;
using PixelBucketWorld.Helpers.Mappers.InputModels.InputInvitationalMessageModels;
using PixelBucketWorld.Helpers.Mappers.OutputModels;
using PixelBucketWorld.Helpers.Paging;
using PixelBucketWorld.Services;
using PixelBucketWorld.Services.Contracts;
using System;
using System.Threading.Tasks;

namespace PixelBucketWorld.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class MessagesController : ControllerBase
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly ILoggingService loggingService;

        public MessagesController(IUnitOfWork unitOfWork, ILoggingService loggingService)
        {
            this.unitOfWork = unitOfWork;
            this.loggingService = loggingService;
        }

        //[Authorize(UserRoleConsts.ORGANISER)]
        //[HttpGet("{id}")]
        //public async Task<IActionResult> GetById(int id)
        //{
        //    var invMessage = await this.unitOfWork.MessageService.GetByIdAsync(id);

        //    if (invMessage == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(new OutputMessagePartial(invMessage));
        //}

        //// api/messages?userId={userId}&contestId={contestId}
        //[Authorize(UserRoleConsts.ORGANISER)]
        //[HttpGet("")]
        //public async Task<IActionResult> Get([FromQuery] int? userId, [FromQuery] int? contestId, [FromQuery] PagingParameters pagingParameters)
        //{
        //    // get the active user
        //    var activeUser = (User)HttpContext.Items["User"];
        //    var userRole = HttpContext.Items["Role"].ToString();

        //    var invMessages = (await this.unitOfWork.MessageService.GetAsync(userId: userId, contestId: contestId))
        //        .Select(m => new OutputMessagePartial(m));

        //    var pagedContests = PagedList<OutputMessagePartial>.ToPagedList
        //        (
        //        invMessages,
        //        pagingParameters.Pagenumber,
        //        pagingParameters.PageSize,
        //        Request
        //        );

        //    WriteHeaders.PagedResposeParameters(Response, pagedContests);

        //    return Ok(pagedContests);
        //}

        /// <summary>
        /// Participants can review their invitational messages.
        /// Token authentication.
        /// </summary>
        /// <param name="contestId"></param>
        /// <param name="pagingParameters"></param>
        /// <response code="200">Returns a collection of all invitational messages.</response>
        /// <response code="401">
        /// If invalid token.
        /// </response>
        [Authorize(UserRoleConsts.PARTICIPANT)]
        [HttpGet("participants")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetParticipantMessages([FromQuery] int? contestId, [FromQuery] PagingParameters pagingParameters)
        {
            // get the active user
            var activeUser = (User)HttpContext.Items["User"];
            var userRole = HttpContext.Items["Role"].ToString();

            //var invMessages = (await this.unitOfWork.MessageService.GetAsync(userId: activeUser.Id, contestId: contestId))
            //    .Select(m => new OutputMessagePartial(m));

            //var pagedContests = PagedList<OutputMessagePartial>.ToPagedList
            //    (
            //    invMessages,
            //    pagingParameters.Pagenumber,
            //    pagingParameters.PageSize,
            //    Request
            //    );

            //WriteHeaders.PagedResposeParameters(Response, pagedContests);

            var invMessages = (await this.unitOfWork.MessageService.GetAsync(userId: activeUser.Id, contestId: contestId));

            //return Ok(pagedContests);
            return Ok(invMessages);
        }


        /// <summary>
        /// Organizers can invite a participant to participate in invitational contest.
        /// Token authentication.
        /// </summary>
        /// <param name="inputModel"></param>
        /// <response code="201">Returns the created message and its url.</response>
        /// <response code="401">If invalid token.</response>
        /// <response code="404">If userId or contestId are invalid.</response>
        /// <response code="400">
        /// If invalid token.
        /// If sending invitation to organizer.
        /// If contest is not invitational.
        /// If contest is not in phase prestart.
        /// If the user already has invitation for the contest.
        /// </response>
        [Authorize(UserRoleConsts.ORGANISER)]
        [HttpPost("participants")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> InviteParticipant([FromBody] InputInivitationalMessage inputModel)
        {
            // get the active user
            var activeUser = (User)HttpContext.Items["User"];

            //var message = InputMapper.Map(inputModel, activeUser);

            try
            {
                var message = await this.unitOfWork.MessageService.CreateParticipationInvitationalMessageAsync(inputModel.ReceiverId, inputModel.ContestId, activeUser.Id);
                await this.unitOfWork.ContestParticipantLinkService.CreateDefaultContestParticipantLink(inputModel.ReceiverId, inputModel.ContestId);
                await this.unitOfWork.SaveAsync();

                // write in logger
                await this.loggingService.LogInformationAsync(InformationLogEvent.INSERT_ITEM, null, message);
                await this.unitOfWork.SaveAsync();

                var route = Request.Path.Value;

                string url = UrlHelper.GetUrl(Request, message.Id);

                return Created(url, new OutputMessageBase(message));

            }
            catch (ArgumentNullException ex)
            {
                return NotFound(ex.Message);
            }
            catch (InvalidOperationException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Organizers can invite a participant to be a juror in contest.
        /// Token authentication.
        /// </summary>
        /// <param name="inputModel"></param>
        /// <response code="201">Returns the created message and its url.</response>
        /// <response code="401">If invalid token.</response>
        /// <response code="404">If userId or contestId are invalid.</response>
        /// <response code="400">
        /// If invalid token.
        /// If sending invitation to organizer.
        /// If user's rank is not Master or Dictator.
        /// If contest is not in phase prestart.
        /// If the user already has invitation for the contest.
        /// </response>
        [Authorize(UserRoleConsts.ORGANISER)]
        [HttpPost("jurors")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> InviteJuror([FromBody] InputInivitationalMessage inputModel)
        {
            // get the active user
            var activeUser = (User)HttpContext.Items["User"];

            //var message = InputMapper.Map(inputModel, activeUser);

            try
            {
                var message = await this.unitOfWork.MessageService.CreateJuryInvitationalMessageAsync(inputModel.ReceiverId, inputModel.ContestId, activeUser.Id);
                await this.unitOfWork.ContestJurorLinkService.CreateDefaultContestJurorLink(message.ReceiverId, message.ContestId);
                await this.unitOfWork.SaveAsync();

                // write in logger
                await this.loggingService.LogInformationAsync(InformationLogEvent.INSERT_ITEM, null, message);
                await this.unitOfWork.SaveAsync();

                var route = Request.Path.Value;

                string url = UrlHelper.GetUrl(Request, message.Id);

                return Created(url, new OutputMessageBase(message));

            }
            catch (ArgumentNullException ex)
            {
                return NotFound(ex.Message);
            }
            catch (InvalidOperationException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Participant can accept invitaions to participate in contest.
        /// Token authentication.
        /// </summary>
        /// <param name="invId"></param>
        /// <response code="200">If operation successfull.</response>
        /// <response code="401">
        /// If invalid token.
        /// If the invitation id does not match any of the participant's invitations.
        /// </response>
        /// <response code="404">If unexisting invitation.</response>
        [Authorize(UserRoleConsts.PARTICIPANT)]
        [HttpPut("participants/{invId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> AcceptParticipantInvitation(int invId)
        {
            try
            {
                // get the active user
                var user = (User)HttpContext.Items["User"];

                var invitation = await this.unitOfWork.MessageService.AcceptParticipantInvitation(invId, user.Id);
                var contestParticipantLink = await this.unitOfWork.ContestParticipantLinkService.GetAsync(user.Id, invitation.ContestId);
                this.unitOfWork.ContestParticipantLinkService.UpdateContestInvitationState(contestParticipantLink);
                await this.unitOfWork.SaveAsync();

                // write in logger
                await this.loggingService.LogInformationAsync(InformationLogEvent.UPDATE_ITEM, "Accept invitation.", invitation);
                await this.unitOfWork.SaveAsync();

                return Ok("Invitation accepted.");
            }
            catch (ArgumentNullException ex)
            {
                return NotFound(ex.Message);
            }
            catch (InvalidOperationException ex)
            {
                return Unauthorized(ex.Message);
            }
        }

        /// <summary>
        /// Participant can accept invitaions to participate in contest.
        /// Token authentication.
        /// </summary>
        /// <param name="invId"></param>
        /// <response code="200">If operation successfull.</response>
        /// <response code="401">
        /// If invalid token.
        /// If the invitation id does not match any of the participant's invitations.
        /// If the participant has already accepted invitation to be juror in the contest.
        /// </response>
        /// <response code="404">If unexisting invitation.</response>
        [Authorize(UserRoleConsts.PARTICIPANT)]
        [HttpPut("jurors/{invId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> AcceptJurorInvitation(int invId)
        {
            try
            {
                // get the active user
                var user = (User)HttpContext.Items["User"];

                var invitation = await this.unitOfWork.MessageService.AcceptJurorInvitationAsync(invId, user.Id);
                var contestJurorLink = await this.unitOfWork.ContestJurorLinkService.GetAsync(invitation.ContestId, user.Id);
                this.unitOfWork.ContestJurorLinkService.UpdateContestInvitationState(contestJurorLink);
                await this.unitOfWork.SaveAsync();

                // write in logger
                await this.loggingService.LogInformationAsync(InformationLogEvent.UPDATE_ITEM, "Accept invitation.", invitation);
                await this.unitOfWork.SaveAsync();

                return Ok("Invitation accepted.");
            }
            catch (ArgumentNullException ex)
            {
                return NotFound(ex.Message);
            }
            catch (InvalidOperationException ex)
            {
                return Unauthorized(ex.Message);
            }
        }

    }
}
