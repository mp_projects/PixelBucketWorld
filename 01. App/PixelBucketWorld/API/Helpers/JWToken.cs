﻿using Microsoft.IdentityModel.Tokens;
using PixeBucketWorld.Data.Models;
using PixelBucketWorld.Services.Helpers;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace PixelBucketWorld.API.Helpers
{
    internal class JWToken
    {
        public JWToken(DateTime validTo, string token)
        {
            this.ValidTo = validTo;
            this.Token = token;
        }

        public DateTime ValidTo { get; set; }

        public string Token { get; set; }

        internal static JWToken GenerateToken(User user, AppSettings appSettings, string role)
        {
            var tokenHandler = new JwtSecurityTokenHandler();

            var key = Encoding.ASCII.GetBytes(appSettings.Secret);

            var claims = new[]
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Role, role)
            };

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var validTo = tokenDescriptor.Expires.Value;
            var token2 = tokenHandler.CreateToken(tokenDescriptor);
            return new JWToken(validTo, tokenHandler.WriteToken(token2));
            //return tokenHandler.WriteToken(token);
        }
    }
}
