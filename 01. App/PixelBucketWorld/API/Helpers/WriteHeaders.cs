﻿using Microsoft.AspNetCore.Http;
using PixelBucketWorld.Helpers.Paging;

namespace PixelBucketWorld.API.Helpers
{
    public static class WriteHeaders
    {
        public static void PagedResposeParameters<T>(HttpResponse response, PagedList<T> pagedResponse)
        {
            response.Headers.Add("HasNext", pagedResponse.HasNext.ToString());
            response.Headers.Add("HasPrevious", pagedResponse.HasPrevious.ToString());
            response.Headers.Add("TotalPages", pagedResponse.TotalPages.ToString());
            response.Headers.Add("TotalRecords", pagedResponse.TotalCount.ToString());
            response.Headers.Add("UrlNext", pagedResponse.UrlNext);
            response.Headers.Add("UrlPrevious", pagedResponse.UrlPrevious);
        }
    }
}
