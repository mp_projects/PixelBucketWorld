﻿using PixeBucketWorld.Data.Models;
using PixelBucketWorld.ViewModels.Base;
using PixelBucketWorld.ViewModels.PhotoVMs;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PixelBucketWorld.ViewModels.ContestVMs
{
    public class ContestPhaseTwoDetailsViewModel : ContestPhaseDetails
    {
        public ContestPhaseTwoDetailsViewModel(Contest contest) : base(contest)
        {
            this.ContestId = contest.Id;
            this.Category = contest.Category.Name;
            this.Title = contest.Name;
            this.CurrentPhaseDiffInMillisec = (contest.PhaseTwoFinishingDate - DateTime.Now).TotalMilliseconds;
            this.Participants = contest.ParticipantsLink.Select(pl => pl.User).ToList();
            this.Jury = contest.JurorsLink.Where(jl => jl.IsAccepted).Select(jl => jl.User).ToList();
            this.Photos = contest.UploadedPhotos.Select(p => new PhotoToRateCardViewModel(p)).ToList();
            this.Type = contest.Type.Name;
        }
        public List<PhotoToRateCardViewModel> Photos { get; set; } = new List<PhotoToRateCardViewModel>();

        public int ContestId { get; set; }

        public double CurrentPhaseDiffInMillisec { get; set; }

        public List<User> Participants { get; set; }

        public List<User> Jury { get; set; }
    }
}
