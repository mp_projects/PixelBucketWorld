﻿using System.ComponentModel.DataAnnotations;

namespace PixelBucketWorld.ViewModels.UserVMs
{
    public class UserLoginViewModel
    {
        [Required]
        public string Username { get; set; }

        [Required, DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "Keep me logged in")]
        public bool isKeptSignedIn { get; set; }
    }
}
