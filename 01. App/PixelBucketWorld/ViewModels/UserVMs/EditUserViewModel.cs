﻿using PixeBucketWorld.Data.Models;
using System.ComponentModel.DataAnnotations;

namespace PixelBucketWorld.ViewModels.UserVMs
{
    public class EditUserViewModel
    {
        public EditUserViewModel()
        {

        }

        public EditUserViewModel(User user)
        {
            this.FirstName = user.FirstName;
            this.LastName = user.LastName;
            this.Password = user.Password;
            this.Email = user.Email;
        }

        [Required, StringLength(20, MinimumLength = 2, ErrorMessage = "First name must be between {1} and {0} characters")]
        public string FirstName { get; set; }

        [Required, StringLength(20, MinimumLength = 2, ErrorMessage = "Last name must be between {1} and {0} characters")]
        public string LastName { get; set; }

        [Required, DataType(DataType.Password)]
        public string Password { get; set; }

        [EmailAddress]
        public string Email { get; set; }
    }
}
