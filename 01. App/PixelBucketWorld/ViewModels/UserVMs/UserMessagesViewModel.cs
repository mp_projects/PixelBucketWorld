﻿using PixelBucketWorld.ViewModels.MessagesVMs;
using System.Collections.Generic;

namespace PixelBucketWorld.ViewModels.UserVMs
{
    public class UserMessagesViewModel
    {
        public List<MessageNotificationViewModel> InvitationalMessages = new List<MessageNotificationViewModel>();
    }
}
