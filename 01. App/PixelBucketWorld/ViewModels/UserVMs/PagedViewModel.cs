﻿using System;
using System.Collections.Generic;

namespace PixelBucketWorld.ViewModels.UserVMs
{
    public class PagedViewModel<T, U>
    {
        public PagedViewModel(List<T> items, int collectionCount, int? pageIndex, int pageSize, U model)
        {
            this.PageIndex = pageIndex ?? 1;
            this.TotalPages = (int)Math.Ceiling(collectionCount / (double)pageSize);
            this.CurrResults = items;
            this.Model = model;
        }

        public IEnumerable<T> CurrResults { get; set; }

        public int PageIndex { get; set; }
        public int TotalPages { get; set; }

        public bool HasPrevious
        {
            get
            {
                return (PageIndex > 1);
            }
        }

        public bool HasNext
        {
            get
            {
                return (PageIndex < TotalPages);
            }
        }

        public List<T> PagedCollection { get; set; }

        public int CollectionCount { get; set; }

        public U Model { get; set; }
    }
}
