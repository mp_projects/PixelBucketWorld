﻿using PixelBucketWorld.ViewModels.ContestVMs;
using PixelBucketWorld.ViewModels.PhotoVMs;
using System.Collections.Generic;

namespace PixelBucketWorld.ViewModels.IndexVMs
{
    public class IndexViewModel
    {
        public int LatestWinningPhotosCount { get; set; }
        public IEnumerable<PhotoCardViewModel> LatestWinningPhotos { get; set; }
        public IEnumerable<ContestCardViewModel> OpenPhaseOneContests { get; set; }
    }
}
