﻿using Microsoft.AspNetCore.Builder;
using PixelBucketWorld.Middlewares;

namespace PixelBucketWorld.Extensions
{
    public static class ExceptionHandlerMiddlewareExtension
    {
        public static void UseExceptionHandlerMiddleware(this IApplicationBuilder app)
        {
            app.UseMiddleware<ExceptionHandlerMiddleware>();
        }
    }
}
