﻿
$(document).ready(function () {
    let value;

    $("#newPhoto").click(function () {

        $("#uploadingPhotoDiv").show();
        $("#previouslyUploadedPhotosDiv").hide();
        $("#photoUrlInputDiv").hide();

        $("#uploadExistingPhotoInput").val('');
        $("#previouslyUploadedPhotosDiv").hide();
        $("#photoUrlInput").val('');
        $("#photoUrlInputDiv").hide();
    })

    $("#photoUrl").click(function () {

        $("#photoUrlInputDiv").show();
        $("#uploadingPhotoDiv").hide();
        $("#previouslyUploadedPhotosDiv").hide();

        $("#uploadExistingPhotoInput").val('');
        $("#previouslyUploadedPhotosDiv").hide();
        $("#uploadNewPhotoInput").val('');
        $("#uploadingPhotoDiv").hide();
    })

    $("#existingPhoto").click(function () {

        $("#previouslyUploadedPhotosDiv").show();
        $("#uploadingPhotoDiv").hide();
        $("#photoUrlInputDiv").hide();

        $("#uploadNewPhotoInput").val('');
        $("#uploadingPhotoDiv").hide();
        $("#photoUrlInput").val('');
        $("#photoUrlInputDiv").hide();
    })
})