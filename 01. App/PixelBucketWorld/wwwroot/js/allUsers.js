﻿$(document).ready(function () {

    var select = $("#contestTypes");

    select.children('option').each(function (index) {
        $(this).attr("id", "typesOption" + index);
    })

    select.change(function () {
        if ($(this).val() == 2) {
           $("#inviteUsersForm").attr("style", "display:block");
        }
        else {
            $("#inviteUsersForm").attr("style", "display:none");
            $("#changeInviteBtn").hide();
        }
    })

    $("#inviteBtn").bind("click", function () {
        $("#inviteUsersForm").attr("style", "display:none");
        $("#changeInviteBtn").attr("style", "display:block");
    })

    $("#changeInviteBtn").bind("click", function () {
        $("#inviteUsersForm").attr("style", "display:block");
        $(this).hide();
    })

    $('.usersToInvite').each(function () {
        $(this).change(function () {
            var userToDisable = $('.usersForJury').find(`[id='${$(this).attr("id")}'`);
            if (userToDisable.length > 0) {
                if ($(this).checked) {
                    userToDisable.attr("disabled", true);
                }
                else {
                    userToDisable.attr("disabled", false);
                }
            }
        })
    })

    $("#inviteJuryBtn").click(function () {
        $("#changeInviteJuryBtn").show();
        $("#juryToInviteForm").hide();
    })
});