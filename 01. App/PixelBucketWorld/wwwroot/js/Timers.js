﻿$(document).ready(() => {
    var timeLeftPhaseI = $("#remainingTimeCurrPhase").html();

    var timeLeftNextPhase = $("#remainingTimeNextPhase").html();

    var timeValueCurrPhase = setInterval(function () {

        if (timeLeftPhaseI <= 1000) {
            clearInterval(timeValueCurrPhase);
        }

        MyCurrPhaseTimer(timeLeftPhaseI);
        timeLeftPhaseI = timeLeftPhaseI - 1000;

    }, 1000);

    var timeValueNextPhase = setInterval(function () {
        if (timeLeftNextPhase <= 1000) {
            clearInterval(timeValueNextPhase);
        };

        MyNextPhaseTimer(timeLeftNextPhase);
        timeLeftNextPhase = timeLeftNextPhase - 1000;
    }, 1000);
})

function MyNextPhaseTimer(timeLeftNextPhase) {
    timeLeftNextPhase = + timeLeftNextPhase;

    var days = Math.floor(timeLeftNextPhase / (1000 * 60 * 60 * 24));
    var hours = Math.floor((timeLeftNextPhase % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((timeLeftNextPhase % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((timeLeftNextPhase % (1000 * 60)) / 1000);

    var daysStr = days + "d";
    var hoursStr = hours + "h";
    var minutesStr = minutes + "m";
    var secondsStr = seconds + "s";

    var finalTime = daysStr + " " + hoursStr + " " + minutesStr + " " + secondsStr;

    $("#remainingTimeNextPhase").html(finalTime);
}

function MyCurrPhaseTimer(timeLeftPhaseI) {

    timeLeftPhaseI = +timeLeftPhaseI;

    var days = Math.floor(timeLeftPhaseI / (1000 * 60 * 60 * 24));
    var hours = Math.floor((timeLeftPhaseI % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((timeLeftPhaseI % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((timeLeftPhaseI % (1000 * 60)) / 1000);

    var daysStr = days + "d";
    var hoursStr = hours + "h";
    var minutesStr = minutes + "m";
    var secondsStr = seconds + "s";

    var finalTime = daysStr + " " + hoursStr + " " + minutesStr + " " + secondsStr;

    $("#remainingTimeCurrPhase").html(finalTime);
}