﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PixeBucketWorld.Data.Models;
using PixeBucketWorld.Data.Models.Logging.Enums;
using PixelBucketWorld.Consts.Numbers;
using PixelBucketWorld.Helpers;
using PixelBucketWorld.Map;
using PixelBucketWorld.Services;
using PixelBucketWorld.Services.Contracts;
using PixelBucketWorld.ViewModels.ContestVMs;
using System;
using System.IO;
using System.Threading.Tasks;

namespace PixelBucketWorld.Areas.PBW.Controllers
{
    [Area("PBW")]
    public class PhotosController : Controller
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly UserManager<User> userManager;
        private readonly ILoggingService loggingService;
        private readonly IWebHostEnvironment webHostingEnvironment;

        public PhotosController(IUnitOfWork unitOfWork, UserManager<User> userManager, ILoggingService loggingService, IWebHostEnvironment webHostingEnvironment)
        {
            this.unitOfWork = unitOfWork;
            this.userManager = userManager;
            this.loggingService = loggingService;
            this.webHostingEnvironment = webHostingEnvironment;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Details(int id)
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(ContestPhaseOneDetailsViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.UploadPhotoVM.Photo != null)
                {
                    //var uploadsFolderShortcut = Path.Combine("wwwroot", "images", "UploadedPhotos");
                    var uploadsFolder = Path.Combine(webHostingEnvironment.WebRootPath, "images", "UploadedPhotos");

                    //Using guid cause more than one photo with the same name can be uploaded
                    var uniqueFileName = Guid.NewGuid().ToString() + "_" + model.UploadPhotoVM.Photo.FileName;

                    var filePath = Path.Combine(uploadsFolder, uniqueFileName);
                    //var filePathShortcut = Path.Combine(uploadsFolderShortcut, uniqueFileName);

                    var photo = InputMapper.Map(model.UploadPhotoVM);

                    var filePathShortcut = $"/images/UploadedPhotos/{uniqueFileName}";
                    photo.PhotoUrl = filePathShortcut;

                    await this.unitOfWork.PhotoService.CreateAsync(photo);

                    await Uploader.Upload(model.UploadPhotoVM.Photo, filePath);

                    if (model.ContestTypeId == ContestTypeConsts.OPEN)
                    {
                        await this.unitOfWork.ContestParticipantLinkService.CreateAsync(new ContestParticipantLink { UserId = photo.UserId, ContestId = photo.ContestId, IsAccepted = null, ParticipationDate = null, UploadingPhotoTime = DateTime.Now });
                    }
                    else
                    {
                       var contestParticipantLink = await this.unitOfWork.ContestParticipantLinkService.GetAsync(photo.UserId, photo.ContestId);
                       this.unitOfWork.ContestParticipantLinkService.UpdatePhotoUploadingState(contestParticipantLink);
                    }

                    await this.unitOfWork.SaveAsync();

                    await this.loggingService.LogInformationAsync(InformationLogEvent.INSERT_ITEM, null, photo);
                    await this.unitOfWork.SaveAsync();

                    // write in logger !

                    return View("SuccessfullyUploadedPhoto", photo);
                }
            }
            else
            {
                ModelState.AddModelError("", "Please provide photo");
            }

            return View("PhaseOneDetails", model);
        }

        public ActionResult Edit(int id)
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Delete(int id)
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
