﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PixeBucketWorld.Data.Models;
using PixelBucketWorld.Services;
using PixelBucketWorld.Services.Contracts;
using PixelBucketWorld.ViewModels.MessagesVMs;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PixelBucketWorld.Areas.PBW.Controllers
{
    [Area("PBW")]
    [Authorize]
    public class MessagesController : Controller
    {
        private readonly UserManager<User> userManager;
        private readonly ILoggingService loggingService;
        private readonly IUnitOfWork unitOfWork;

        public MessagesController(UserManager<User> userManager, ILoggingService loggingService, IUnitOfWork unitOfWork)
        {
            this.userManager = userManager;
            this.loggingService = loggingService;
            this.unitOfWork = unitOfWork;
        }

        [HttpGet]
        public async Task<IActionResult> GetUserMessages(int id)
        {
            var user = await userManager.FindByIdAsync(id.ToString());

            var messages = await this.unitOfWork.MessageService.GetUserInvitationalMessagesAsync(user.Id);

            var messageNotificationsVM = messages.Select(m => new MessageNotificationViewModel(m));

            return View(messageNotificationsVM);
        }

        [HttpGet]
        public async Task<IActionResult> Get(int id)
        {
            var message = await this.unitOfWork.MessageService.GetByIdAsync(id);
            var user = await userManager.GetUserAsync(this.User);
          

            if(message.ReceiverId != user.Id)
            {
                return View("PageNotFound");
            }

            var messageVM = new MessageViewModel(message);

            var userJurorLink = await this.unitOfWork.ContestJurorLinkService.GetAsync(user.Id, message.ContestId);

            if(userJurorLink == null)
            {
                var userParticipantLink = await this.unitOfWork.ContestParticipantLinkService.GetAsync(user.Id, message.ContestId);

                if(userParticipantLink==null)
                {
                    throw new InvalidOperationException();
                }

                messageVM.isContestAccepted = userParticipantLink.IsAccepted.Value;
            }
            else
            {
                messageVM.isContestAccepted = userJurorLink.IsAccepted;
                messageVM.IsUserJuror = true;
            }

            return View(messageVM);
        }
    }
}
