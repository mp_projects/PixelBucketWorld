﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using PixeBucketWorld.Data.Models;
using PixeBucketWorld.Data.Models.FilteringModels;
using PixeBucketWorld.Data.Models.Logging.Enums;
using PixelBucketWorld.Consts.Numbers;
using PixelBucketWorld.Consts.Strings;
using PixelBucketWorld.Helpers;
using PixelBucketWorld.Map;
using PixelBucketWorld.Services;
using PixelBucketWorld.Services.Contracts;
using PixelBucketWorld.ViewModels.ContestVMs;
using PixelBucketWorld.ViewModels.PhotoAssessmentVMs;
using PixelBucketWorld.ViewModels.PhotoVMs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace PixelBucketWorld.Areas.PBW.Controllers
{
    [Area("PBW")]
    public class ContestsController : Controller
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly UserManager<User> userManager;
        private readonly ILoggingService loggingService;
        private readonly IWebHostEnvironment webHostingEnvironment;

        public ContestsController(IUnitOfWork unitOfWork, UserManager<User> userManager, ILoggingService loggingService, IWebHostEnvironment webHostingEnvironment)
        {
            this.unitOfWork = unitOfWork;
            this.userManager = userManager;
            this.loggingService = loggingService;
            this.webHostingEnvironment = webHostingEnvironment;
        }

        [HttpGet]
        [Authorize(Roles = UserRoleConsts.ORGANISER)]
        public async Task<IActionResult> Create()
        {
            var createContestVM = new CreateContestViewModel();

            await this.FillCreateView(createContestVM);

            return View(createContestVM);
        }

        [HttpPost]
        [Authorize(Roles = UserRoleConsts.ORGANISER)]
        public async Task<IActionResult> Create(CreateContestViewModel model, IEnumerable<int> usersToInvite, IEnumerable<int> usersForJury)
        {
            if (ModelState.IsValid)
            {
                var sameUsers = usersToInvite.Intersect(usersForJury).ToList();

                if (sameUsers.Any())
                {
                    List<string> users = new List<string>();

                    foreach (var userId in sameUsers)
                    {
                        var user = await this.unitOfWork.UserService.GetByIdAsync(userId);
                        users.Add(user.UserName);
                    }


                    if (users.Count == 1)
                    {
                        ModelState.AddModelError("", $"User {users[0]} can be added as jury or participant, but not both");
                    }
                    else
                    {
                        ModelState.AddModelError("", $"User {string.Join(',', users)} can be added as jury or participant, but not both");
                    }

                    await this.FillCreateView(model);

                    return View(model);
                }


                var contest = InputMapper.Map(model);

                if (model.UploadedCoverPhoto != null)
                {
                    //var uploadsFolderShortcut = Path.Combine("wwwroot", "images", "UploadedPhotos");
                    var uploadsFolder = Path.Combine(webHostingEnvironment.WebRootPath, "images", "ContestPhotos");

                    //Using guid cause more than one photo with the same name can be uploaded
                    var uniqueFileName = Guid.NewGuid().ToString() + "_" + model.UploadedCoverPhoto.FileName;

                    var filePath = Path.Combine(uploadsFolder, uniqueFileName);
                    //var filePathShortcut = Path.Combine(uploadsFolderShortcut, uniqueFileName);

                    var filePathShortcut = $"/images/ContestPhotos/{uniqueFileName}";

                    await Uploader.Upload(model.UploadedCoverPhoto, filePath);

                    contest.PhotoUrl = filePathShortcut;
                }
                else if(model.ExistingCoverPhoto != null)
                {
                    var photoUrl = $"/images/ContestPhotos/{model.ExistingCoverPhoto.FileName}";

                    if(!(await this.unitOfWork.PhotoService.DoesCoverPhotoExistsLocally(photoUrl)))
                    {
                        ModelState.AddModelError("", "Please attach existing photo! Navigate to wwwroot/ContestPhotos");

                        await this.FillCreateView(model);

                        return View(model);
                    }

                    contest.PhotoUrl = photoUrl;
                }
                else if(model.PhotoUrl != null)
                {
                    var responseMessage =  await this.unitOfWork.PhotoService.GetRequestResponse(model.PhotoUrl);

                    if (responseMessage == null)
                    {
                        ModelState.AddModelError("", "The URL you've provided is not valid!");

                        await this.FillCreateView(model);

                        return View(model);
                    }

                    var isValid = this.unitOfWork.PhotoService.IsResponseContentTypeValidImageFormat(responseMessage);

                    if (!isValid)
                    {
                        ModelState.AddModelError("", "Only .jpeg and .png formats are accepted");

                        await this.FillCreateView(model);

                        return View(model);
                    }

                    contest.PhotoUrl = model.PhotoUrl;
                }
                else
                {
                    contest.PhotoUrl = "/images/ContestPhotos/PixelBucketWorld.jpg";
                }

                await this.unitOfWork.ContestService.CreateAsync(contest);

                await this.unitOfWork.SaveAsync();

                var sender = await userManager.GetUserAsync(this.User);

                if (sender == null)
                {
                    throw new ArgumentNullException();
                }

                if (usersToInvite != null)
                {
                    //Won't find correct last contest in next methods because SaveChanges() is not called yet??? how to fix
                    await this.unitOfWork.ContestParticipantLinkService.CreateDefaultContestParticipantLinks(usersToInvite, contest.Id);

                    await this.unitOfWork.MessageService.SendParticipantsInvitationalMessageAsync(usersToInvite, contest.Id, sender.Id);

                    await this.unitOfWork.SaveAsync();
                }

                if (usersForJury != null)
                {
                    await this.unitOfWork.MessageService.SendJuryInvitationalMessageAsync(usersForJury, contest.Id, sender.Id);

                    await this.unitOfWork.ContestJurorLinkService.CreateDefaultContestJurorLinks(usersForJury, contest.Id);

                    await this.unitOfWork.SaveAsync();
                }

                await this.loggingService.LogInformationAsync(InformationLogEvent.INSERT_ITEM, null, contest);

                TempData["contestSuccessfullyCreated"] = $"Contest \"{contest.Name}\" has been successfully created";

                return RedirectToAction("OrganiserDashboard", "Users", new { id = sender.Id });
            }

            await this.FillCreateView(model);

            return View(model);
        }

        private async Task FillCreateView(CreateContestViewModel createContestVM)
        {
            //var categories = await this.unitOfWork.ContestService.GetCategoriesAsync();
            var categories = await this.unitOfWork.CategoryService.GetAllAsync();
            //var types = await this.unitOfWork.ContestService.GetTypesAsync();
            var types = await this.unitOfWork.ContestTypesService.GetAllAsync();

            foreach (var category in categories)
            {
                createContestVM.Categories.Add(new SelectListItem { Text = category.Name, Value = $"{category.Id}" });
            }

            foreach (var type in types)
            {
                createContestVM.Types.Add(new SelectListItem { Text = type.Name, Value = $"{type.Id}" });
            }

            var filteringCriterias = new UserFilteringCriterias();
            filteringCriterias.RoleSelectOptions.Add(UserRoles.PARTICIPANT_ROLE_ID);
            var allParticipantUsers = await this.unitOfWork.UserService.GetAsync(filteringCriterias);
            createContestVM.UsersToInvite.AddRange(allParticipantUsers);

            var userFilteringCriterias = new UserFilteringCriterias();
            userFilteringCriterias.RankSelectOptions.Add(RankConsts.MASTER);
            userFilteringCriterias.RankSelectOptions.Add(RankConsts.WISE_AND_BENEVOLENT_PHOTO_DICTATOR);
            var participantsEligibleJury = await this.unitOfWork.UserService.GetAsync(userFilteringCriterias);
            createContestVM.Jury.AddRange(participantsEligibleJury);
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Details(int id)
        {
            var contest = await this.unitOfWork.ContestService.GetByIdAsync(id);

            if (contest.CurrentPhase.Id == PhaseConsts.PRESTART)
            {
                ContestPhasePrestartViewModel phasePrestartVM = new ContestPhasePrestartViewModel(contest);
                return View("PhasePrestartDetails", model: phasePrestartVM);
            }
            else if (contest.CurrentPhase.Id == PhaseConsts.PHASE_ONE) //must be changed to 2
            {
                var phaseIDetaislVM = new ContestPhaseOneDetailsViewModel(contest);
                return View("PhaseOneDetails", model: phaseIDetaislVM);
            }
            else if (contest.CurrentPhase.Id == PhaseConsts.PHASE_TWO)
            {
                var phaseTwoDetailsVM = new ContestPhaseTwoDetailsViewModel(contest);
                return View("PhaseTwoDetails", model: phaseTwoDetailsVM);
            }
            else if (contest.CurrentPhase.Id == PhaseConsts.TRANSITION)
            {
                var phaseTransitionDetailsVM = new ContestPhaseTransitionViewModel(contest);
                phaseTransitionDetailsVM.UploadedPhotos = contest.UploadedPhotos.Select(up => new PhotoCardViewModel(up)).ToList();
                return View("PhaseTransitionDetails", model: phaseTransitionDetailsVM);
            }
            else
            {
                var phaseFinishedDetailsVM = new ContestPhaseFinishedViewModel(contest);

                var user = await this.userManager.GetUserAsync(this.User);
                phaseFinishedDetailsVM.IsUserJuror = await unitOfWork.UserService.IsUserJurorAsync(user.Id, contest.Id);

                if (this.User.IsInRole(UserRoleConsts.PARTICIPANT) && !phaseFinishedDetailsVM.IsUserJuror)
                {
                    var userContestPhotoAssessment = await this.unitOfWork.PhotoService.GetUserContestPhotoAsync(contest.Id, user.Id);
                    phaseFinishedDetailsVM.CurrentUserPhotoAssessment = new AssessmentViewModel(userContestPhotoAssessment);
                }

                var firstPlaceWinners = await this.unitOfWork.PhotoService.GetByContestRanking(contest.Id, 1);
                var secondPlaceWinners = await this.unitOfWork.PhotoService.GetByContestRanking(contest.Id, 2);
                var thirdPlaceWinners = await this.unitOfWork.PhotoService.GetByContestRanking(contest.Id, 3);
                var contestLoosersPhotoAssessments = await this.unitOfWork.PhotoService.GetContestNonWinningPhotosAsync(contest.Id);


                phaseFinishedDetailsVM.FirstPlaceWinnersAssessments = firstPlaceWinners.Select(pa => new AssessmentViewModel(pa));
                phaseFinishedDetailsVM.SecondPlaceWinnersAssessments = secondPlaceWinners.Select(pa => new AssessmentViewModel(pa));
                phaseFinishedDetailsVM.ThirdPlaceWinnersAssessments = thirdPlaceWinners.Select(pa => new AssessmentViewModel(pa));
                phaseFinishedDetailsVM.LoosersPhotoAssessments = contestLoosersPhotoAssessments.Select(pa => new AssessmentViewModel(pa));


                return View("PhaseFinishedDetails", phaseFinishedDetailsVM);
            }
        }
    }
}
