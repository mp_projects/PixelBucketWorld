﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PixeBucketWorld.Data.Models;
using PixeBucketWorld.Data.Models.FilteringModels;
using PixeBucketWorld.Data.Models.Logging.Enums;
using PixelBucketWorld.Consts.Numbers;
using PixelBucketWorld.Consts.Strings;
using PixelBucketWorld.Helpers.Mappers.InputModels.InputContestParticipantsContesterModels;
using PixelBucketWorld.Map;
using PixelBucketWorld.Services;
using PixelBucketWorld.Services.Contracts;
using PixelBucketWorld.ViewModels.ContestVMs;
using PixelBucketWorld.ViewModels.UserVMs;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PixelBucketWorld.Areas.PBW.Controllers
{
    [Area("PBW")]
    public class UsersController : Controller
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly UserManager<User> userManager;
        private readonly SignInManager<User> signInManager;
        private readonly RoleManager<Role> roleManager;
        private readonly ILoggingService loggingService;

        public UsersController(IUnitOfWork unitOfWork, UserManager<User> userManager,
            SignInManager<User> signInManager, RoleManager<Role> roleManager, ILoggingService loggingService)
        {
            this.unitOfWork = unitOfWork;
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.roleManager = roleManager;
            this.loggingService = loggingService;
        }

        [HttpPost]
        public async Task<IActionResult> Logout()
        {
            await signInManager.SignOutAsync();

            return RedirectToAction("Index", "Home", new { area = "" });
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Login()
        {
            if(signInManager.IsSignedIn(this.User))
            {
                return RedirectToAction("Index", "Home", new { area = "" });
            }

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Login(UserLoginViewModel loginUser, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var result = await signInManager.PasswordSignInAsync(loginUser.Username, loginUser.Password, loginUser.isKeptSignedIn, false);

                if (result.Succeeded)
                {
                    if (returnUrl != null)
                    {
                        return Redirect(returnUrl);
                    }

                    var user = await userManager.FindByNameAsync(loginUser.Username);
                    var role = await userManager.GetRolesAsync(user);
                    var mainRole = role.First();

                    if (mainRole == UserRoleConsts.PARTICIPANT)
                    {
                        return RedirectToAction("Dashboard", new { id = user.Id });
                    }
                    else
                    {
                        return RedirectToAction("OrganiserDashboard", new { Id = user.Id });
                    }
                }

                ModelState.AddModelError(string.Empty, "Invalid login attempt");

            }

            return View(loginUser);
        }

        [Authorize(Roles = UserRoleConsts.PARTICIPANT)]
        public async Task<IActionResult> Dashboard(int id)
        {
            var user = await this.unitOfWork.UserService.GetByIdAsync(id); // ?? why not expecting int rather than string?

            if (user == null)
            {
                throw new ArgumentNullException();
            }

            await this.unitOfWork.UserService.LoadRankAsync(user);

            var pointsTillNextRank = await this.unitOfWork.UserService.GetPointsTillNextRankAsync(user.Id);

            var onGoingContest = await this.unitOfWork.ContestService.GetParticipantOnGoingContestAsync(user.Id);
            var finishedContests = await this.unitOfWork.ContestService.GetParticipantFinishedContestAsync(user.Id);
            var openContests = await this.unitOfWork.ContestService.GetActiveOpenContestsAsync2(user.Id);

            var dashboardVM = new DashboardViewModel();

            if (await this.unitOfWork.UserService.IsUserJurorAsync(user.Id))
            {
                var finishedContestsAsJuror = await this.unitOfWork.ContestService.GetJurorFinishedContests(user.Id);
                var onGoingContestsAsJuror = await this.unitOfWork.ContestService.GetJurorOnGoingContests(user.Id);
                var activeContestsAsJuror = await this.unitOfWork.ContestService.GetJurorActiveContests(user.Id);

                dashboardVM.FinishedContestsAsJuror.AddRange(finishedContestsAsJuror.Select(c => new ContestCardViewModel(c)));
                dashboardVM.OnGoingContestsAsJuror.AddRange(onGoingContestsAsJuror.Select(c => new ContestCardViewModel(c)));
                dashboardVM.ActiveContestsAsJuror.AddRange(activeContestsAsJuror.Select(c => new ContestCardViewModel(c)));
            }

            dashboardVM.FinishedContests.AddRange(finishedContests.Select(c => new ContestCardViewModel(c)));
            dashboardVM.OnGoingContests.AddRange(onGoingContest.Select(c => new ContestCardViewModel(c)));
            dashboardVM.ActiveContests.AddRange(openContests.Select(c => new ContestCardViewModel(c)));
            dashboardVM.CurrentRank = user.Rank.Name;
            dashboardVM.PointsTillNextRank = pointsTillNextRank;
            dashboardVM.RankingsPoints = user.CurrentRankingPoints.Value;

            return View(dashboardVM);
        }

        [Authorize(Roles = UserRoleConsts.ORGANISER)]
        public async Task<IActionResult> OrganiserDashboard(int id)
        {
            var user = await this.unitOfWork.UserService.GetByIdAsync(id);

            var phasePreContests = await this.unitOfWork.ContestService.GetContestsByPhaseAndTypeAsync(null, PhaseConsts.PRESTART);
            var phaseIContests = await this.unitOfWork.ContestService.GetContestsByPhaseAndTypeAsync(null, PhaseConsts.PHASE_ONE);
            var phaseTransContests = await this.unitOfWork.ContestService.GetContestsByPhaseAndTypeAsync(null, PhaseConsts.TRANSITION);
            var phaseIIContests = await this.unitOfWork.ContestService.GetContestsByPhaseAndTypeAsync(null, PhaseConsts.PHASE_TWO);
            var finishedContests = await this.unitOfWork.ContestService.GetContestsByPhaseAndTypeAsync(null, PhaseConsts.FINISHED);

            var organiserDashboard = new OrganisersDashboardViewModel();

            organiserDashboard.PhasePrestartContests.AddRange(phasePreContests.Select(c => new ContestCardViewModel(c)));
            organiserDashboard.PhaseOneContests.AddRange(phaseIContests.Select(c => new ContestCardViewModel(c)));
            organiserDashboard.PhaseTransitionContests.AddRange(phaseTransContests.Select(c => new ContestCardViewModel(c)));
            organiserDashboard.PhaseTwoContests.AddRange(phaseIIContests.Select(c => new ContestCardViewModel(c)));
            organiserDashboard.FinishedContests.AddRange(finishedContests.Select(c => new ContestCardViewModel(c)));

            return View(organiserDashboard);
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Register()
        {
            return View();

        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Register(UserRegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = InputMapper.Map(model);
                var result = await userManager.CreateAsync(user, model.Password);

                if (result.Succeeded)
                {
                    this.unitOfWork.UserService.SetUserInitialDetails(user);
                    await this.userManager.AddToRoleAsync(user, UserRoleConsts.PARTICIPANT);
                    await signInManager.SignInAsync(user, false);

                    await loggingService.LogInformationAsync(InformationLogEvent.INSERT_ITEM, null, user);

                    await this.unitOfWork.SaveAsync();

                    TempData["isRegistered"] = "User has been successfully registered";

                    return RedirectToAction("Dashboard", new { Id = user.Id });
                }

                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError("", error.Description);
                }
            }

            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> Profile(string id)
        {
            var user = await userManager.FindByIdAsync(id);

            if (user == null)
            {
                throw new ArgumentNullException();
            }

            var editUserVM = new EditUserViewModel(user);

            return View(editUserVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Profile([FromForm] EditUserViewModel inputUser, string id)
        {
            var user = await userManager.GetUserAsync(this.User);
            var validPassword = await userManager.CheckPasswordAsync(user, inputUser.Password);

            if (!validPassword)
            {
                ModelState.AddModelError("", "Invalid password");
                return View(inputUser);
            }

            if (user == null)
            {
                return View("PageNotFound");
            }

            var editedUser = InputMapper.Map(inputUser, user);

            var result = await this.userManager.UpdateAsync(editedUser);

            //mapper need a lot of models to map - problem or not?(generic mapper ? :D)

            if (result.Succeeded)
            {
                var detailedMessage = this.unitOfWork.UserService.GetEntityUpdatesDetailedMessage(user);
                await loggingService.LogInformationAsync(InformationLogEvent.UPDATE_ITEM, detailedMessage, user);
                await this.unitOfWork.SaveAsync();

                TempData["successfullyUpdated"] = "Your profile is sucessfully updated!";

                return RedirectToAction("Index", "Home", new { area = " " });
            }

            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error.Description);
            }

            return View(inputUser);
        }

        [Authorize(Roles = UserRoleConsts.ORGANISER)]
        public async Task<IActionResult> GetAll()
        {
            var users = await userManager.Users.ToListAsync();

            if (users.Count == 0)
            {
                ViewBag["NoUsers"] = "No users have been found";
            }

            var usersViewModel = users.Select(u => new UserViewModel(u));

            return View(usersViewModel);
        }

        [HttpGet]
        [Authorize(Roles = UserRoleConsts.ORGANISER)]
        public async Task<IActionResult> Filtrate()
        {
            var ranks = await this.unitOfWork.RankService.GetAllAsFilteringCategoryAsync();
            var roles = await this.unitOfWork.RoleService.GetAllAsFilteringCategoryAsync();

            var userFiltrationVM = new UserFiltrationViewModel();

            userFiltrationVM.Categories.Add("Rank", ranks.ToList());
            userFiltrationVM.Categories.Add("Role", roles.ToList());

            return View(userFiltrationVM);
        }

        [HttpGet]
        [Authorize(Roles = UserRoleConsts.ORGANISER)]
        public async Task<IActionResult> GetFiltered(UserFilteringCriterias userFiltering, int? page)
        {
            var filteredUsers = await this.unitOfWork.UserService.GetAsync(userFiltering);

            var userVM = filteredUsers.Select(u => new UserViewModel(u));

            ViewData["userFilteringCriterias"] = userFiltering;

            return View("GetAll", model: userVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(string id)
        {
            var user = await userManager.FindByIdAsync(id);

            if (user == null)
            {
                return View("NotFound");
            }

            var result = await userManager.DeleteAsync(user);

            if (result.Succeeded)
            {
                await this.unitOfWork.SaveAsync();

                await this.loggingService.LogInformationAsync(InformationLogEvent.DELETE_ITEM, null, user);
                await this.unitOfWork.SaveAsync();

                return RedirectToAction("Home", "Index");
            }

            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error.Description);
            }

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> AcceptContestInvitation(InputAcceptedContestInvitation inputModel)
        {
            var contestParticipantLink = await this.unitOfWork.ContestParticipantLinkService.GetAsync(inputModel.UserId,inputModel.ContestId);

            if (contestParticipantLink == null)
            {
                var contestJurorLink = await this.unitOfWork.ContestJurorLinkService.GetAsync(inputModel.UserId, inputModel.ContestId);

                if (contestJurorLink == null)
                {
                    throw new InvalidOperationException();
                }

                this.unitOfWork.ContestJurorLinkService.UpdateContestInvitationState(contestJurorLink);
            }
            else
            {
                this.unitOfWork.ContestParticipantLinkService.UpdateContestInvitationState(contestParticipantLink);
            }

            await this.unitOfWork.SaveAsync();

            TempData["invitationAccepted"] = "Contest invitation accepted";

            return RedirectToAction("GetUserMessages", "Messages", new { Id = inputModel.UserId });
        }
    }
}