﻿using System.ComponentModel.DataAnnotations;

namespace PixelBucketWorld.Map.InputModels.InputUserModels
{
    public class InputRegisterUser
    {
        [Required, StringLength(20, MinimumLength = 2, ErrorMessage = "First name must be between {1} and {0} characters")]
        public string FirstName { get; set; }

        [Required, StringLength(20, MinimumLength = 2, ErrorMessage = "Last name must be between {1} and {0} characters")]
        public string LastName { get; set; }

        [Required, DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        public string Username { get; set; }

        public string Email { get; set; }
    }
}
