﻿namespace PixelBucketWorld.Helpers.Mappers.InputModels.InputInvitationalMessageModels
{
    public class InputInivitationalMessage
    {
        public int ReceiverId { get; set; }

        public int ContestId { get; set; }
    }
}
