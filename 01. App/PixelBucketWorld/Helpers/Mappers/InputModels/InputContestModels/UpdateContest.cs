﻿using System;

namespace PixelBucketWorld.Helpers.Mappers.InputModels.InputContestModels
{
    public class UpdateContest
    {
        public string Title { get; set; }

        public int? CategoryId { get; set; }

        public int? TypeId { get; set; }

        public DateTime? PhaseOneStartingDate { get; set; }

        public DateTime? PhaseOneFinishingDate { get; set; }

        public DateTime? PhaseTwoStartingDate { get; set; }

        public DateTime? PhaseTwoFinishingDate { get; set; }

    }
}
