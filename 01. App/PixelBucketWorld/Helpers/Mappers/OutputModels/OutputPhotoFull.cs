﻿using Microsoft.AspNetCore.Http;
using PixeBucketWorld.Data.Models;
using System.Collections.Generic;
using System.Linq;

namespace PixelBucketWorld.Helpers.Mappers.OutputModels
{
    public class OutputPhotoFull : OutputPhotoPartial
    {
        public OutputPhotoFull(Photo photo, HttpRequest request)
            : base(photo, request)
        {
            this.Participant = new OutputUserPartial(photo.User);
            this.FinalPoints = photo.FinalPoints;
            this.Ranking = photo.ContestRanking;
            this.Assesments = photo.PhotoAssessments
                .Select(a => new OutputPhotoAssesmentPartial(a)).ToList();
        }

        public OutputUserPartial Participant { get; set; }

        //public OutputContestPartial Contest { get; set; }

        public double FinalPoints { get; set; }

        public int? Ranking { get; set; }

        public List<OutputPhotoAssesmentPartial> Assesments { get; set; } 
            = new List<OutputPhotoAssesmentPartial>();
    }
}
