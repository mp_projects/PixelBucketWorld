﻿using PixeBucketWorld.Data.Models;

namespace PixelBucketWorld.Helpers.Mappers.OutputModels
{
    public class OutputMessagePartial : OutputMessageBase
    {
        public OutputMessagePartial()
        {

        }

        public OutputMessagePartial(InvitationalMessage invMessage)
            :base(invMessage)
        {
            this.Receiver = new OutputUserBase(invMessage.Receiver);
            this.Sender = new OutputUserBase(invMessage.Sender);
            this.ContestAccepted = invMessage.ContestAccepted;
            this.HasBeenViewed = invMessage.HasBeenViewed;
            this.Contest = new OutputContestBase(invMessage.Contest);
        }


        public OutputUserBase Receiver { get; set; }

        public OutputUserBase Sender { get; set; }


        public bool ContestAccepted { get; set; }

        public bool HasBeenViewed { get; set; }

        public OutputContestBase Contest { get; set; }


    }
}
