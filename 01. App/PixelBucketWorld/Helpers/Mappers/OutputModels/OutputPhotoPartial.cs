﻿using Microsoft.AspNetCore.Http;
using PixeBucketWorld.Data.Models;

namespace PixelBucketWorld.Helpers.Mappers.OutputModels
{
    public class OutputPhotoPartial
    {
        public OutputPhotoPartial(Photo photo)
        {
            Id = photo.Id;
            Title = photo.Title;
            Story = photo.Story;
        }

        public OutputPhotoPartial(Photo photo, HttpRequest request)
        {
            Id = photo.Id;
            Title = photo.Title;
            Story = photo.Story;
            PhotoUrl = Url(request);
        }


        public int Id { get; set; }

        public string Title { get; set; }

        public string Story { get; set; }

        public string PhotoUrl { get; set; }

        private string Url(HttpRequest request)
        {
            string scheme = request.Scheme;
            string host = request.Host.Value;
            return $"{scheme}://{host}/api/photos/{this.Id}";

        }
    }
}
