﻿using PixeBucketWorld.Data.Models;
using PixelBucketWorld.Map.OutputModels;
using System.Collections.Generic;
using System.Linq;

namespace PixelBucketWorld.Helpers.Mappers.OutputModels
{
    public class OutputContestFull : OutputContestPartial
    {
        public OutputContestFull(Contest contest)
            : base(contest)
        {
            Participants = contest.ParticipantsLink.Select(j => new OutputUserPartial(j.User)).ToList();
            Jury = contest.JurorsLink.Select(j => new OutputUserPartial(j.User)).ToList();
            UploadedPhotos = contest.UploadedPhotos.Select(j => new OutputPhotoPartial(j)).ToList();
        }

        public List<OutputUserPartial> Participants { get; set; }

        public List<OutputUserPartial> Jury { get; set; } = new List<OutputUserPartial>();
        
        public List<OutputPhotoPartial> UploadedPhotos { get; set; } = new List<OutputPhotoPartial>();
        
    }
}
