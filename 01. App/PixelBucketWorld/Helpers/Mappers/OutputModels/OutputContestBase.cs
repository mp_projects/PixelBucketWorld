﻿using PixeBucketWorld.Data.Models;

namespace PixelBucketWorld.Helpers.Mappers.OutputModels
{
    public class OutputContestBase
    {
        public OutputContestBase()
        {

        }

        public OutputContestBase(Contest contest)
        {
            Id = contest.Id;
            Title = contest.Name;
        }

        public int Id { get; set; }

        public string Title { get; set; }

    }
}
