﻿using PixeBucketWorld.Data.Models;
using PixelBucketWorld.Helpers.Mappers.OutputModels;
using PixelBucketWorld.Map.OutputModels;

namespace PixelBucketWorld.Map
{
    public static class OutputMapper
    {
        public static OutputContestPartial Map(Contest contest)
        {
            return new OutputContestPartial()
            {
                Id = contest.Id,
                Type = contest.Type.Name,
                Category = contest.Category.Name,
                Title = contest.Name,
                PhaseOneStartingDate = contest.PhaseOneStartingDate,
                PhaseOneFinishingDate = contest.PhaseOneFinishingDate,
                PhaseTwoStartingDate = contest.PhaseTwoStartingDate,
                PhaseTwoFinishingDate = contest.PhaseTwoFinishingDate,
                CurrentPhase = contest.CurrentPhase.Name
            };
        }

        public static OutputUserPartial Map(User user)
        {
            OutputUserPartial outputUser = new OutputUserPartial()
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
            };

            if (user.Rank != null)
            {
                outputUser.Rank = user.Rank.Name;
            }
            else
            {
                outputUser.Rank = "";
            }

            return outputUser;
        }

        public static OutputCategoryPartial Map(Category category)
        {
            return new OutputCategoryPartial()
            {
                Id = category.Id,
                Name = category.Name
            };
        }
    }
}
