﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PixeBucketWorld.Data.Models;

namespace PixeBucketWorld.Data.ModelsConfig
{
    class UserConfig : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.Property(u => u.Email).IsRequired();
            builder.Property(u => u.UserName).IsRequired();
            builder.HasIndex(u => u.UserName).IsUnique();
            builder.HasMany(u => u.SentMessages).WithOne(sm => sm.Sender);
            builder.HasMany(u => u.ReceivedMessages).WithOne(rm => rm.Receiver);
            builder.Property(u => u.Email).IsRequired(false);
        }
    }
}
