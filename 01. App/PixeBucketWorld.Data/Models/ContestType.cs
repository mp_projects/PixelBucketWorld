﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PixeBucketWorld.Data.Models
{
    public class ContestType
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public List<Contest> Contests { get; set; } = new List<Contest>();
    }
}
