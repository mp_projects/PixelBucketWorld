﻿using Microsoft.AspNetCore.Identity;
using PixeBucketWorld.Data.Contracts;

namespace PixeBucketWorld.Data.Models
{
    public class Role : IdentityRole<int>, IUserFilteringCategory
    {

    }
}
