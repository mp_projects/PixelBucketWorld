﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PixeBucketWorld.Data.Models
{
    public class PhotoAssessment
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [Range(0, 10, ErrorMessage = "Score must be in range 0 to 10!")]
        public int Score { get; set; }

        [Required]
        public string Comment { get; set; }

        [ForeignKey("UserId")]
        public int UserId { get; set; }

        public User User { get; set; }

        [ForeignKey("PhotoId")]
        public int PhotoId { get; set; }

        public Photo Photo { get; set; }
    }
}
