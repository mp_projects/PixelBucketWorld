﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PixeBucketWorld.Data.Models.FilteringModels
{
    public class UserFilteringCriterias
    {
        public List<int> RankSelectOptions { get; set; } = new List<int>();
        public List<int> RoleSelectOptions { get; set; } = new List<int>();

        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
