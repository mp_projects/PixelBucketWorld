﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PixeBucketWorld.Data.Models
{
    public class User : IdentityUser<int>
    {
        ////Provided from the base functionality
        //[Required]
        //public string Username { get; set; }

        ////Provided from the base functionality
        //public int RoleId { get; set; }

        ////Provided from the base functionality
        //public Role Role { get; set; }

        [Required, StringLength(20, MinimumLength = 2, ErrorMessage = "First name must be between {1} and {0} characters")]
        public string FirstName { get; set; }

        [Required, StringLength(20, MinimumLength = 2, ErrorMessage = "Last name must be between {1} and {0} characters")]
        public string LastName { get; set; }

        public string Password { get; set; }

        public int? RankId { get; set; }

        public Rank Rank { get; set; }

        public int? CurrentRankingPoints { get; set; }

        public List<Photo> Photos { get; set; }

        public List<ContestParticipantLink> ContestParticipantLinks { get; set; }

        public List<ContestJurorLink> ContestJurorLinks { get; set; }

        public List<InvitationalMessage> ReceivedMessages { get; set; }

        public List<InvitationalMessage> SentMessages { get; set; }

        public List<PhotoAssessment> PhotoAssessments { get; set; }
    }
}
