﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PixeBucketWorld.Data.Models
{
    public class Photo
    {
        [Key]
        public int Id { get; set; }

        [Required, MinLength(3, ErrorMessage = "Photo title must be at least {1} characters")]
        public string Title { get; set; }

        [Required, MinLength(8, ErrorMessage = "Photo story must be at least {1} characters")]
        public string Story { get; set; }

        [Required]
        public string PhotoUrl { get; set; }

        [ForeignKey("UserId")]
        public int UserId { get; set; }

        public User User { get; set; }

        [ForeignKey("ContestId")]
        public int ContestId { get; set; }

        public Contest Contest { get; set; }
        
        public int? ContestRanking { get; set; }

        public double FinalPoints { get; set; }

        public List<PhotoAssessment> PhotoAssessments { get; set; } = new List<PhotoAssessment>();
    }
}
