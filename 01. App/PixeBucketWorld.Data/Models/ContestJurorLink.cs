﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PixeBucketWorld.Data.Models
{
    public class ContestJurorLink
    {
        [ForeignKey("UserId")]
        public int UserId { get; set; }

        public User User { get; set; }

        [ForeignKey("ContestId")]
        public int ContestId { get; set; }

        public Contest Contest { get; set; }

        public bool IsAccepted { get; set; }

        public DateTime? AcceptanceTime { get; set; }
    }
}
