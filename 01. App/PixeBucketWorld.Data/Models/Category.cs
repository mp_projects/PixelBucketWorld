﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PixeBucketWorld.Data.Models
{
    public class Category
    {
        [Key]
        public int Id { get; set; }

        [Required, StringLength(30, MinimumLength = 3, ErrorMessage = "Category name must be between {1} and {0} characters")]
        public string Name { get; set; }

        public List<Contest> Contests { get; set; } = new List<Contest>();
    }
}
