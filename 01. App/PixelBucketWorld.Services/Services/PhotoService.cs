﻿using Microsoft.EntityFrameworkCore;
using PixeBucketWorld.Data;
using PixeBucketWorld.Data.Models;
using PixelBucketWorld.Consts.Numbers;
using PixelBucketWorld.Consts.Strings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace PixelBucketWorld.Services.Services
{
    public class PhotoService : GenericService<Photo>
    {

        public PhotoService(PBWDbContext context)
            : base(context)
        {
        }

        public async Task<Photo> GetByIdAsync(int id, bool load)
        {
            var photo = await base.GetByIdAsync(id);

            if (photo == null)
            {
                throw new ArgumentNullException();
            }

            if (load)
            {
                this.context.Entry(photo).Reference(p => p.User).Query().Include(u => u.Rank).Load();
                this.context.Entry(photo).Reference(p => p.Contest).Load();
                this.context.Entry(photo).Collection(p => p.PhotoAssessments).Query()
                    .Include(a => a.User)
                        .ThenInclude(u => u.Rank)
                    .Load();
            }

            return photo;
        }

        public async Task<IEnumerable<Photo>> GetAsync(int? contestId = null)
        {
            IQueryable<Photo> query = base.dbSet;

            if (contestId != null)
            {
                query = query.Where(photo => photo.ContestId == contestId);
            }

            return await query.ToListAsync();
        }

        public async Task<Photo> GetUserContestPhotoAsync(int contestId, int userId)
        {
            var photo = await this.dbSet.Include(p => p.PhotoAssessments).Include(p => p.User)
                .FirstOrDefaultAsync(p => p.ContestId == contestId && p.UserId == userId);
            
            return photo;
        }

        public async Task<IEnumerable<Photo>> GetContestNonWinningPhotosAsync
            (int contestId)
        {
            var photos = await this.dbSet
                .Include(p => p.PhotoAssessments).Include(p => p.User)
                .Where(p => p.ContestId == contestId && p.ContestRanking == null)
                .ToListAsync();

            return photos;
        }


        public async Task<IEnumerable<Photo>> GetLatestWinningPhotoAsync(int number)
        {
            IQueryable<Photo> query = base.dbSet
                .Where(photo => photo.ContestRanking == 1)
                .Include(photo => photo.Contest)
                .OrderByDescending(photo => photo.Contest.PhaseTwoFinishingDate)
                .Take(number);

            return await query.ToListAsync();
        }

        public override Photo Edit<InputModel>(InputModel updated, int id)
        {
            var photo = base.Edit(updated, id);

            context.Entry(photo).Reference(p => p.User).Load();

            return photo;
        }

        public override async Task<Photo> CreateAsync(Photo entity)
        {
            // validate contest is in phase I
            var contest = await this.context.Contests
                .FirstOrDefaultAsync(c => c.Id == entity.ContestId);

            if (contest == null)
            {
                throw new ArgumentNullException(string.Format(ErrorConsts.ENTITY_NOT_FOUND, "Contest", entity.ContestId));
            }
            if (contest.PhaseId != PhaseConsts.PHASE_ONE)
            {
                throw new InvalidOperationException("Can't upload. The contest is not in phase I.");
            }

            // if the user has been invited as juror - validate if he has accepted the invitation
            var contestJurorLink = await this.context.ContestJurorLinks
                .FirstOrDefaultAsync(jl => jl.UserId == entity.UserId && jl.ContestId == entity.ContestId);

            if (contestJurorLink != null && contestJurorLink.IsAccepted)
            {
                throw new InvalidOperationException("Can't upload. You have accepted to be a juror in this contest.");
            }


            // if contest is invitational - validate: 1) the user has been invited; 2) he has accepted the invitation
            if (contest.TypeId == ContestTypeConsts.INVITATIONAL)
            {
                var contestParticipantLink = await this.context.ContestParticipantLinks
                    .FirstOrDefaultAsync(jl => jl.UserId == entity.UserId && jl.ContestId == entity.ContestId);

                if (contestParticipantLink == null)
                {
                    throw new InvalidOperationException("Can't upload. You are not invited.");
                }
                if (contestParticipantLink.IsAccepted == false)
                {
                    throw new InvalidOperationException("You have got an invitation. Please accept it first.");
                }
            }

            // validete single photo upload from that user
            var existingPhoto = await this.dbSet
                .FirstOrDefaultAsync(p => p.UserId == entity.UserId && p.ContestId == entity.ContestId);
            if (existingPhoto != null)
            {
                throw new InvalidOperationException("You have already uploaded photo for this contest. Only one upload is permitted.");
            }

            var photo = await base.CreateAsync(entity);

            this.context.Entry(photo).Reference(p => p.Contest).Load();

            context.Entry(photo).Reference(p => p.User).Query().Include(u => u.Rank).Load();

            return photo;
        }

        public async Task<IEnumerable<Photo>> GetHistoryAsync()
        {
            var photos = this.dbSet
                .Include(p => p.Contest)
                .Include(p => p.User)
                    .ThenInclude(u => u.Rank)
                .Include(p => p.PhotoAssessments)
                .Where(p => p.Contest.PhaseId == 5);

            return await photos.ToListAsync();
        }

        public async Task<IEnumerable<Photo>> GetByContestRanking(int contestId, int rankingPlace)
        {
            var photos = await this.dbSet.Where(p => p.ContestId == contestId && p.ContestRanking == rankingPlace).ToListAsync();
            return photos;
        }

        public async Task<bool> DoesCoverPhotoExistsLocally(string photoUrl)
        {
            if (await this.context.Contests.AnyAsync(c => c.PhotoUrl == photoUrl))
            {
                return true;
            }

            return false;
        }

        public async Task<HttpResponseMessage> GetRequestResponse(string photoUrl)
        {
            try
            {
                var client = new HttpClient();
                var result = await client.SendAsync(new HttpRequestMessage(HttpMethod.Head, photoUrl));

                if (result.IsSuccessStatusCode)
                {
                    return result;
                }

                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool IsResponseContentTypeValidImageFormat(HttpResponseMessage message)
        {
            var mediaType = message.Content.Headers.ContentType.MediaType;

            if (mediaType == "image/jpeg" || mediaType == "image/png")
            {
                return true;
            }

            return false;
        }
    }
}
