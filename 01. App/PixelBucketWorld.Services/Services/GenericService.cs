﻿using Microsoft.EntityFrameworkCore;
using PixeBucketWorld.Data;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PixelBucketWorld.Services.Services
{
    public class GenericService<T>
        where T : class
    {    
        protected readonly PBWDbContext context;
        protected DbSet<T> dbSet;

        public GenericService(PBWDbContext context)
        {
            this.context = context;
            this.dbSet = this.context.Set<T>();
        }

        public virtual async Task<T> CreateAsync(T entity)
        {
            await this.dbSet.AddAsync(entity);
            return entity;
        }

        public virtual async Task<T> DeleteAsync(int id)
        {
            T entity = await this.dbSet.FindAsync(id);

            if (entity == null)
            {
                return null;
            }

            try
            {
                this.dbSet.Remove(entity);
            }
            catch
            {
                throw new DataException();
            }

            return entity;
        }

        public virtual async Task<IEnumerable<T>> GetAllAsync()
        {
            IQueryable<T> query = this.dbSet;
            return await query.ToListAsync();
        }

        public virtual async Task<T> GetByIdAsync(int id)
        {
            T entity = await this.dbSet.FindAsync(id);

            return entity;
        }

        public virtual T Edit<InputModel> (InputModel updated, int id)
        {
            if (updated == null)
                return null;

            T existing = dbSet.Find(id);

            if (existing != null)
            {
                var entry = context.Entry(existing);
                var properties = updated.GetType().GetProperties();

                foreach (var prop in properties)
                {
                    var propValue = prop.GetValue(updated);

                    if (propValue != null)
                    {
                        entry.Property(prop.Name).CurrentValue = propValue;
                    }
                }
            }

            return existing;
        }

        public virtual async Task<T> UpdateAsync<InputModel>(InputModel updated, int id)
        {
            if (updated == null)
                return null;

            T existing = await dbSet.FindAsync(id);

            if (existing != null)
            {
                var entry = context.Entry(existing);
                var properties = updated.GetType().GetProperties();

                foreach (var prop in properties)
                {
                    var propValue = prop.GetValue(updated);

                    if (propValue != null)
                    {
                        entry.Property(prop.Name).CurrentValue = propValue;
                    }
                }
            }

            return existing;
        }

        public string GetEntityUpdatesDetailedMessage(T entity)
        {
            var messageBuilder = new StringBuilder();

            var entry = context.Entry(entity);

            foreach(var item in entry.CurrentValues.Properties)
            {
                var propEntry = entry.Property(item.Name);

                if(propEntry.IsModified)
                {
                    messageBuilder.Append($"{item.Name} has been changed from {propEntry.OriginalValue} to {propEntry.CurrentValue}, "); 
                }
            }

           return messageBuilder.ToString().TrimEnd(' ', ',');
        }
    }
}
