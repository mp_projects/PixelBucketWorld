﻿using Microsoft.EntityFrameworkCore;
using PixeBucketWorld.Data;
using PixeBucketWorld.Data.Contracts;
using PixeBucketWorld.Data.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PixelBucketWorld.Services.Services
{
    public class RankService : GenericService<Rank>
    {
        public RankService(PBWDbContext context)
            : base(context)
        {

        }

        public async Task<IEnumerable<IUserFilteringCategory>> GetAllAsFilteringCategoryAsync()
        {
            var ranks = await base.GetAllAsync();
            return ranks;
        }

    }
}
