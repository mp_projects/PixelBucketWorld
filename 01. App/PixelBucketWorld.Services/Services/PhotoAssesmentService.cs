﻿using Microsoft.EntityFrameworkCore;
using PixeBucketWorld.Data;
using PixeBucketWorld.Data.Models;
using PixelBucketWorld.Consts.Strings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PixelBucketWorld.Services.Services
{
    public class PhotoAssesmentService : GenericService<PhotoAssessment>
    {
        public PhotoAssesmentService(PBWDbContext context)
            : base(context)
        {
        }

        public override async Task<PhotoAssessment> GetByIdAsync(int id)
        {
            var photoAssesment = await this.dbSet.FindAsync(id);

            if (photoAssesment != null)
            {
                this.context.Entry(photoAssesment).Reference(a => a.User).Query().Include(u => u.Rank).Load();
                this.context.Entry(photoAssesment).Reference(a => a.Photo).Load();
            }

            return photoAssesment;
        }

        public override async Task<PhotoAssessment> CreateAsync(PhotoAssessment newAssesment)
        {
            var photo = await this.context.Photos.FindAsync(newAssesment.PhotoId);
            if (photo == null)
            {
                throw new ArgumentNullException("Photo not found.");
            }

            if (await IsInvalidContestPhaseAsync(photo))
            {
                throw new InvalidOperationException("Can't asses. Contest is not in phase II.");
            }

            if (await IsInvalideRaterAsync(newAssesment, photo.ContestId))
            {
                throw new UnauthorizedAccessException(
                    "Not a valid jury-member. Check if you have got and accepted invitation.");
            }

            if (await IsRatedAsync(newAssesment))
            {
                throw new InvalidOperationException("Can't asses. You already did it.");
            }

            if (newAssesment.Score < 0 || newAssesment.Score > 10)
            {
                throw new InvalidOperationException("Score must be form 0 to 10.");
            }

            if (newAssesment.Score != 0 && string.IsNullOrEmpty(newAssesment.Comment))
            {
                throw new InvalidOperationException("Comment is required.");
            }

            if (newAssesment.Score == 0)
            {
                newAssesment.Comment = AssesmentConstants.DEFAULT_COMMENT_FOR_ZERO_SCORE;
            }

            return await base.CreateAsync(newAssesment);

        }

        private async Task<bool> IsInvalidContestPhaseAsync(Photo photo)
        {
            var contestPhaseId = (await this.context.Contests
                .FirstOrDefaultAsync(c => c.Id == photo.ContestId)).PhaseId;
            if (contestPhaseId != 4)
            {
                return true;
            }

            return false;
        }

        private async Task<bool> IsInvalideRaterAsync(PhotoAssessment newAssesment, int contestId)
        {
            // check if assessor is Organiser (rank == null)
            var user = await this.context.Users
                .Where(u => u.Rank == null)
                .FirstOrDefaultAsync(u => u.Id == newAssesment.UserId);

            if (user == null)
            {
                // if assessor is not Organiser - check if he has been included in jury
                var jurrorLink = await this.context.ContestJurorLinks
                    .FirstOrDefaultAsync(x => x.UserId == newAssesment.UserId && x.ContestId == contestId);

                if (jurrorLink == null || !jurrorLink.IsAccepted)
                {
                    return true;
                }
            }

            return false;
        }

        private async Task<bool> IsRatedAsync(PhotoAssessment newAssesment)
        {
            var assesment = await this.dbSet
                .FirstOrDefaultAsync(a => a.PhotoId == newAssesment.PhotoId && a.UserId == newAssesment.UserId);

            if (assesment != null)
            {
                return true;
            }

            return false;
        }
    }
}
