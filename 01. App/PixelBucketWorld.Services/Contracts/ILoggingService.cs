﻿using PixeBucketWorld.Data.Models.Logging.Enums;
using System;
using System.Threading.Tasks;

namespace PixelBucketWorld.Services.Contracts
{
    public interface ILoggingService 
    {
        public Task LogInformationAsync<T>(InformationLogEvent logEvent, string detailedMessage, T model);

        public Task LogWarningAsync<T>(WarningLogEvent logEvent, string detailedMessage, T model);

        Task LogErrorAsync(Exception e);
    }
}
